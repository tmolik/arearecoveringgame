﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInputController : MonoBehaviour
{
    [SerializeField]
    private Camera gameCamera = null;

    [SerializeField]
    private UiDirectionCircleController circleController;

    public Vector3 GetCurrentDirectionVector()
    {
        if (circleController != null)
        {
            return circleController.GetDirectionVector();
        }
        return Vector3.zero;
    }

    public void Update()
    {
#if UNITY_ANDROID
        TouchUpdate();
#else
        TouchImitatorUpdate();
#endif
    }

    private void TouchUpdate()
    {
        if (gameCamera == null)
            return;

        if (Input.touchCount > 0)
        {

            for (int i = 0; i < Input.touchCount; i++)
            {
                if (Input.touches[i].phase == TouchPhase.Began)
                {
                    Ray ray = gameCamera.ScreenPointToRay(Input.touches[i].position);
                    RaycastHit raycastHit;
                    if (Physics.Raycast(ray, out raycastHit, 100))
                    {
                        if (raycastHit.collider != null)
                        {
                            ITouchable touchableObject = raycastHit.collider.GetComponent<ITouchable>();
                            if (touchableObject != null)
                            {
                                touchableObject.OnTouch();
                            }
                        }
                    }
                }
            }
        }
    }

    private void TouchImitatorUpdate()
    {
        if (gameCamera == null)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = gameCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit, 100))
            {
                if (raycastHit.collider != null)
                {
                    ITouchable touchableObject = raycastHit.collider.GetComponent<ITouchable>();
                    if (touchableObject != null)
                    {
                        touchableObject.OnTouch();
                    }
                }
            }
        }
    }
}
