﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeController : MonoBehaviour
{

    [SerializeField]
    private float lifeTime = 3f;

    [SerializeField]
    private float range = 2f;

    [SerializeField]
    private float flySpeed = 3f;

    [SerializeField]
    private float screenShakeTime = 0.3f;
    [SerializeField]
    private float screenShakePower = 0.5f;

    private float flyTime = 0;
    private Vector3 destinationPosition;
    private float helperTimer = 0;

    public void OnDisable()
    {
        helperTimer = 0;
    }

    public void SetDestinationPosition(Vector3 destPosition)
    {
        destinationPosition = destPosition;
        flyTime = Vector3.Distance(destinationPosition, transform.position) / flySpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, destinationPosition)>0.1f)
        {
            transform.position += (destinationPosition - transform.position).normalized * flySpeed * Time.deltaTime;
            transform.position += Vector3.up * 3 * Mathf.Sin(0.2f + helperTimer / flyTime*Mathf.PI) * Time.deltaTime;
        }

        helperTimer += Time.deltaTime;
        if(helperTimer>=lifeTime)
        {
            Explode();
        }
    }

    public void Explode()
    {
        CreateExplosionEffect();
        AddCameraShake();

        Collider[] colliders = Physics.OverlapSphere(transform.position, range);

        foreach (Collider coll in colliders)
        {
            PlayerController player = coll.GetComponent<PlayerController>();
            if(player)
            {
                Levels.LevelController.Get.PlayerCaught(EPlayerCaughtSource.GrenadeExplosion);
                break;
            }
        }
        gameObject.SetActive(false);
    }

    private void CreateExplosionEffect()
    {
        GameObject explosionGameObject = ObjectPooler.Get.GetPooledObject(EPoolables.Explosion);
        explosionGameObject.transform.position = transform.position;
        explosionGameObject.SetActive(true);
    }

    private void AddCameraShake()
    {
        if(Levels.LevelController.Get!=null && Levels.LevelController.Get.FollowingCamera != null)
        {
            Levels.LevelController.Get.FollowingCamera.Shake(screenShakeTime, screenShakePower);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player)
        {
            Explode();
        }
    }
}
