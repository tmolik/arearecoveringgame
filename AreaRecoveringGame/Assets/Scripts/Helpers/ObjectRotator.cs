﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotateAxis;

    [SerializeField]
    private float rotateSpeed = 30;

    // Update is called once per frame
    void Update()
    {
        Vector3 eulerRotation = transform.rotation.eulerAngles + rotateAxis * rotateSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(eulerRotation);
    }
}
