﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateAfterTime : MonoBehaviour
{
    [SerializeField]
    private float deactivationTime = 5f;

    public void OnEnable()
    {
        StartCoroutine(Deactivate());
    }

    public void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(5f);
        gameObject.SetActive(false);
    }
}
