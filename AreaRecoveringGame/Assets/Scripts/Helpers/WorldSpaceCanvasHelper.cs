﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSpaceCanvasHelper : MonoBehaviour
{

    private Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Levels.LevelController.Get.FollowingCamera.CameraComponent;
    }

    // Update is called once per frame
    void Update()
    {
        if (mainCamera == null)
            return;
        Vector3 lookDirection = mainCamera.transform.position - transform.position;
        lookDirection.Normalize();
        lookDirection *= -1;
        transform.rotation = Quaternion.LookRotation(lookDirection);
        //transform.LookAt(mainCamera.transform);
        //ransform.rotation = mainCamera.transform.rotation;
    }
}
