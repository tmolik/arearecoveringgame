﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class PlayParticleAtRandomTimes : MonoBehaviour
{

    private ParticleSystem particleSystemToPlay = null;

    [SerializeField]
    private Vector2 timeRange = new Vector2(1, 4);

    private Vector2 localScaleRange = new Vector2(0.15f,0.4f);

    public void Awake()
    {
        particleSystemToPlay = GetComponent<ParticleSystem>();
    }


    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(WaitAndPlay());
    }


    private IEnumerator WaitAndPlay()
    {
        yield return new WaitForSeconds(timeRange.GetRandomValueInRange());
        transform.localScale = Vector3.one * localScaleRange.GetRandomValueInRange();
        particleSystemToPlay.Play(true);
        StartCoroutine(WaitAndPlay());
    }

}
