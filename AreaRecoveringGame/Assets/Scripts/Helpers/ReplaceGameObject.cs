﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplaceGameObject : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> newGameObjectPrefabs = new List<GameObject>();

    public void Replace()
    {
        GameObject newObject = Instantiate(newGameObjectPrefabs.GetRandom(), transform.parent);
        newObject.transform.position = transform.position;
        newObject.transform.rotation = transform.rotation;
        newObject.name = name;

        DestroyImmediate(gameObject);
    }
}
