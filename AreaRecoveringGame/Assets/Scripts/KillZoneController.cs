﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZoneController : MonoBehaviour
{
    public int KillZoneId = 0;

    [SerializeField]
    private float timeToKill = 5f;

    private int currentAreasCount = 0;
    private float helperTimer = 0;

    public void Start()
    {
        helperTimer = timeToKill;
    }

    public void PlayerEnteredKillZone(PlayerController player)
    {
        currentAreasCount++;
        player.EnteredKillZone(timeToKill);
    }

    public void PlayerLeftKillZone(PlayerController player)
    {
        currentAreasCount--;
        player.SpecialEffectsController.LeftKillZone();
        if (currentAreasCount <= 0)
        {
            helperTimer = timeToKill;
            player.ExitedKillZone();
        }
    }

    public void Update()
    {
        if (currentAreasCount > 0)
        {
            helperTimer -= Time.deltaTime;
            if (helperTimer <= 0)
            {
                Kill();
            }
        }
    }

    public void Kill()
    {
        if (Levels.LevelController.Get)
            Levels.LevelController.Get.PlayerCaught(EPlayerCaughtSource.KillZone);
    }
}
