﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Levels
{
    public class LevelSelectController : MonoBehaviour
    {
        [SerializeField]
        private List<LevelPreset> levelPresets = new List<LevelPreset>();

        [SerializeField]
        private GameObject levelToSelectPrefab = null;

        public void SpawnLevelsToSelect()
        {
            ClearLevelsInChildren();
            if (levelToSelectPrefab == null)
            {
                Debug.LogError("Level to select prefab is null");
                return;
            }

            for (int i = 0; i <= levelPresets.Count - 1; i++)
            {
                GameObject levelToSelectGameObject = Instantiate(levelToSelectPrefab);
                LevelToSelectController levelToSelect = levelToSelectGameObject.GetComponent<LevelToSelectController>();
                levelToSelect.levelPreset = levelPresets[i];
                levelToSelect.transform.position = new Vector3(Mathf.Sin(i)*2.5f, 0, 2 + i * 2f);
                levelToSelect.transform.SetParent(transform);
            }
        }

        private void ClearLevelsInChildren()
        {
            foreach (LevelToSelectController levelSelector in GetComponentsInChildren<LevelToSelectController>())
            {
                Destroy(levelSelector.gameObject);
            }
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}