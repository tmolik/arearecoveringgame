﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Levels
{
    public class LevelToSelectController : MonoBehaviour, ITouchable
    {
        [SerializeField]
        public LevelPreset levelPreset = null;

        private bool isAvailable = false;

        [SerializeField]
        private MeshRenderer levelRenderer = null;

        [SerializeField]
        private Material lockedMaterial = null;
        [SerializeField]
        private Material unlockedMaterial = null;

        [SerializeField]
        private bool alwaysEnabledForTest = false;

        [SerializeField]
        private GameObject parawanWallGameobject;

        public void Start()
        {
            CheckIfIsAvailable();
        }

        public void OnTouch()
        {
            if (!isAvailable)
                return;

            if (LevelsManager.Get != null)
                LevelsManager.Get.SelectedLevel(levelPreset);
        }

        public void Awake()
        {
            SaveManager.OnLoad += CheckIfIsAvailable;
        }

        public void OnDestroy()
        {
            SaveManager.OnLoad -= CheckIfIsAvailable;
        }

        private void SetAvailability(bool available)
        {
            if (alwaysEnabledForTest)
                available = true;

            if (levelRenderer)
                levelRenderer.material = available ? unlockedMaterial : lockedMaterial;
            isAvailable = available;
        }

        private void CheckIfIsAvailable()
        {
            if (levelPreset == null)
                return;

            SetAvailability(levelPreset.LevelID <= SaveManager.GameData.FinishedLevels + 1);
            SetParawanWall();
        }

        private void SetParawanWall()
        {
            if (parawanWallGameobject == null || levelPreset == null)
                return;

            parawanWallGameobject.SetActive(levelPreset.LevelID == SaveManager.GameData.FinishedLevels);
        }

        public void OnMouseEnter()
        {
            Debug.Log("Hoveruje level " + levelPreset.LevelID);
        }

        public void OnMouseExit()
        {
            Debug.Log("Przestałem hoverować level " + levelPreset.LevelID);
        }


    }
}