﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Levels
{
    public class LevelsManager : MonoBehaviour
    {
        public static LevelsManager Get { get; private set; }

        public LevelsManager()
        {
            Get = this;
        }

        [SerializeField]
        private List<LevelPreset> levelPresets = new List<LevelPreset>();

        [HideInInspector]
        public LevelPreset CurrentlyPlayedLevel;

        public void SelectedLevel(LevelPreset levelPreset)
        {
            if (levelPreset == null)
                return;

            if (!levelPresets.Contains(levelPreset))
                return;

            LoadLevel(levelPreset);
        }

        private void LoadLevel(LevelPreset levelPreset)
        {
            if (string.IsNullOrWhiteSpace(levelPreset.SceneName))
                return;

            if (ScenesManager.Get != null)
                ScenesManager.Get.LoadScene(levelPreset.SceneName);

            CurrentlyPlayedLevel = levelPreset;
        }

        public void ResetLevel()
        {
            if (CurrentlyPlayedLevel == null)
                return;

            LoadLevel(CurrentlyPlayedLevel);
        }

        public void WonCurrentLevel()
        {
            if (CurrentlyPlayedLevel == null)
                return;
            if(CurrentlyPlayedLevel.LevelID > SaveManager.GameData.FinishedLevels)
            {
                SaveManager.GameData.FinishedLevels = CurrentlyPlayedLevel.LevelID;
                SaveManager.Get.SaveGame();
            }
        }

        public void LoadNextLevel()
        {
            if (CurrentlyPlayedLevel == null)
                return;

            int nextLevelId = CurrentlyPlayedLevel.LevelID + 1;

            for (int i = 0; i <= levelPresets.Count - 1; i++)
            {
                if (levelPresets[i].LevelID == nextLevelId)
                {
                    LoadLevel(levelPresets[i]);
                    return;
                }
            }

        }
    }
}