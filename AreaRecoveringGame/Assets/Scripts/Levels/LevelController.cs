﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cameras;
using UnityEngine.AI;
using UI;

namespace Levels
{
    public class LevelController : MonoBehaviour
    {
        #region Singleton

        public static LevelController Get { get; private set; }

        public LevelController()
        {
            Get = this;
        }

        #endregion

        [SerializeField]
        public AreasManager AreasManager = null;

        [SerializeField]
        private PlayerController player = null;

        [SerializeField]
        private AreaController playerStartArea = null;

        [SerializeField]
        public FollowingCameraController FollowingCamera = null;

        //[HideInInspector]
        public int CurrentLives = 0;
        public int MaxLives = 3;

        [HideInInspector]
        public UnityEvent OnPlayerLifesCountChange = new UnityEvent();

        private bool gameInProgress = false;

        // Start is called before the first frame update
        void Start()
        {
            StartGame();
        }

        // Update is called once per frame
        void Update()
        {
            if (!gameInProgress)
                return;
            DebugInputUpdate();
        }

        private void DebugInputUpdate()
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                GameWon();
            }
        }

        public void PlayerCaught(EPlayerCaughtSource source)
        {
            if (CurrentLives <= 0)
                return;

            if (player.IsInvulnerable)
            {
                Debug.Log("Oszukał śmierć");
                return;
            }
            AreasManager.ResetAreasToFree(EAreaStatus.TemporaryTakenByDrawing);

            CurrentLives--;
            OnPlayerLifesCountChange.Invoke();
            if (CurrentLives <= 0)
            {
                StartCoroutine(GameLost());
            }
            else
            {
                player.ResetPlayer();
                player.SpecialEffectsController.UnequipBall();
                player.SpecialEffectsController.UnequipUmbrella();
                player.transform.position = playerStartArea.transform.position;
                UI.UiGameController.Get.ShowCaughtText();
            }
        }

        public void PlayerFinishedRecovering(AreaController finishArea)
        {
            AreasManager.PlayerFinishedTakingAreas();
            player.transform.position = new Vector3(finishArea.transform.position.x, player.transform.position.y, finishArea.transform.position.z);
            player.ResetPlayer();
        }

        public PlayerController GetPlayer()
        {
            return player;
        }

        public void GameWon()
        {
            if (UiController.Get != null && UiController.Get.SummaryPanel != null)
            {
                UiController.Get.SummaryPanel.Show();
                UiController.Get.SummaryPanel.Setup(true);
            }
            if (LevelsManager.Get != null)
                LevelsManager.Get.WonCurrentLevel();
            gameInProgress = false;

        }

        private IEnumerator GameLost()
        {
            gameInProgress = false;
            player.Die();
            yield return new WaitForSeconds(4f);
            if (UiController.Get != null && UiController.Get.SummaryPanel != null)
            {
                UiController.Get.SummaryPanel.Show();
                UiController.Get.SummaryPanel.Setup(false);
            }
        }

        public void StartGame()
        {
            if (player == null)
            {
                Debug.LogError("Player null");
                return;
            }
            player.transform.position = playerStartArea.transform.position;
            player.gameObject.SetActive(true);

            if (FollowingCamera != null)
                FollowingCamera.SetupTarget(player.transform);

            CurrentLives = MaxLives;
            OnPlayerLifesCountChange.Invoke();

            gameInProgress = true;
        }

        public void CollectedLife()
        {
            CurrentLives = Mathf.Min(CurrentLives + 1, MaxLives);
            OnPlayerLifesCountChange.Invoke();
        }

        public void SetAreaAsStarting(AreaController area)
        {
            playerStartArea = area;
        }


    }
}