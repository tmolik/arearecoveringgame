﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Levels
{
    [CreateAssetMenu(fileName = "LevelPreset", menuName = "Presets/Level Preset")]
    public class LevelPreset : ScriptableObject
    {
        public int LevelID;
        public string SceneName;
    }
}