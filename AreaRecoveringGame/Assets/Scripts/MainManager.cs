﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainManager
{
    private const string managerPath = "MainManager";
    private const string uiControllerPath = "UiController";

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    private static void Init()
    {
        try
        {
            SpawnMainManager();
        }
        catch (System.Exception e)
        {
            Debug.Log("Error loading main manager " + e.Message);
        }
    }

    private static void SpawnMainManager()
    {
        Object mainManagerPrefab = Resources.Load(managerPath);
        Object spawnedObject = Object.Instantiate(mainManagerPrefab);
        spawnedObject.name = mainManagerPrefab.name;

        Object.DontDestroyOnLoad(spawnedObject);
    }
}
