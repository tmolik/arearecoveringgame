﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionChangingObjectController : MonoBehaviour
{
    [SerializeField]
    private List<Transform> positions = new List<Transform>();

    [SerializeField]
    private float timeToSpendOnPosition = 5f;

    private int currentPositionIndex = 0;
    private float helperTimer = 0;

    private bool blocked = false;

    // Update is called once per frame
    void Update()
    {
        if (blocked)
            return;

        helperTimer += Time.deltaTime;
        if (helperTimer >= timeToSpendOnPosition)
            SelectNewPosition();
    }

    public void SelectNewPosition()
    {
        if (positions.Count == 0)
            return;
        if (positions[currentPositionIndex] == null)
            return;

        currentPositionIndex = (currentPositionIndex + 1) % positions.Count;
        transform.position = positions[currentPositionIndex].position;
        transform.rotation = positions[currentPositionIndex].rotation;
        helperTimer = 0;
    }

    public void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player)
        {
            blocked = true;
        }

    }

    public void OnTriggerExit(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player)
        {
            blocked = false;
        }

    }
}
