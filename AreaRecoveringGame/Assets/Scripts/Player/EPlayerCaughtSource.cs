﻿public enum EPlayerCaughtSource
{
    Trap,
    EnemyCaughtWindScreens,
    GrenadeExplosion,
    KillZone,
    Fall,
    PlayerEnteredWindScreen,
    DeadlyTrigger
}
