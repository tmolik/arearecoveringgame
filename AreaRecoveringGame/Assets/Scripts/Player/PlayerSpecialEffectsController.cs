﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpecialEffectsController
{
    [System.NonSerialized]
    public PlayerController Player;

    private Dictionary<EAreaSpecialEffectType, int> currentSpecialEffects = new Dictionary<EAreaSpecialEffectType, int>();

    [HideInInspector]
    public float SpeedDecreaseValue = 0f;

    [System.NonSerialized]
    public Transform UmbrellaTransform;
    [System.NonSerialized]
    public bool HasBouncingUmbrella = false;
    private float bouncingUmbrellaTimer = 0;

    [System.NonSerialized]
    public Transform OffensiveBallTransform;
    [System.NonSerialized]
    public bool HasOffensiveBall = false;


    public void UpdateEffects()
    {
        if (HasBouncingUmbrella)
        {
            bouncingUmbrellaTimer -= Time.deltaTime;
            if (bouncingUmbrellaTimer <= 0)
            {
                UnequipUmbrella();
            }
        }

        if (HasOffensiveBall && Input.GetKeyDown(KeyCode.LeftControl))
        {
            ThrowBall();
        }
    }

    public void EquipUmbrella(float time)
    {
        bouncingUmbrellaTimer = time;
        UmbrellaTransform.gameObject.SetActive(true);
        HasBouncingUmbrella = true;
        UI.UiSpecialEffectsController.Get.AddEffectIndicator(EEffectType.Umbrella, -1f);
    }

    public void UnequipUmbrella()
    {
        UmbrellaTransform.gameObject.SetActive(false);
        HasBouncingUmbrella = false;
        UI.UiSpecialEffectsController.Get.RemoveEffectIndicator(EEffectType.Umbrella);
    }

    public void EquipBall()
    {
        HasOffensiveBall = true;
        OffensiveBallTransform.gameObject.SetActive(true);
        UI.UiSpecialEffectsController.Get.AddEffectIndicator(EEffectType.OffensiveBall, -1f);
    }

    public void ThrowBall()
    {
        UnequipBall();
        GameObject ballGameObject = ObjectPooler.Get.GetPooledObject(EPoolables.OffensiveBallController);
        OffensiveBallController ballController = ballGameObject.GetComponent<OffensiveBallController>();
        ballController.transform.position = OffensiveBallTransform.position;
        ballController.transform.rotation = OffensiveBallTransform.rotation;
        ballController.gameObject.SetActive(true);
        ballController.ThrowedAt(Player.transform.forward);
    }

    public void UnequipBall()
    {
        HasOffensiveBall = false;
        OffensiveBallTransform.gameObject.SetActive(false);
        UI.UiSpecialEffectsController.Get.RemoveEffectIndicator(EEffectType.OffensiveBall);

    }

    public void EnteredSlowEffect(float speedDecreaseValue)
    {
        if (!currentSpecialEffects.ContainsKey(EAreaSpecialEffectType.SlowEffect))
            currentSpecialEffects.Add(EAreaSpecialEffectType.SlowEffect, 0);

        currentSpecialEffects[EAreaSpecialEffectType.SlowEffect] += 1;
        SpeedDecreaseValue = speedDecreaseValue;
        UI.UiSpecialEffectsController.Get.AddEffectIndicator(EEffectType.SlowEffect, -1f);
    }

    public void LeftSlowEffect()
    {
        if (!currentSpecialEffects.ContainsKey(EAreaSpecialEffectType.SlowEffect))
            return;

        currentSpecialEffects[EAreaSpecialEffectType.SlowEffect] -= 1;
        if (currentSpecialEffects[EAreaSpecialEffectType.SlowEffect] <= 0)
        {
            SpeedDecreaseValue = 0;
            UI.UiSpecialEffectsController.Get.RemoveEffectIndicator(EEffectType.SlowEffect);
        }
    }

    public void EnteredKillZone(float speedDecreaseValue)
    {
        if (!currentSpecialEffects.ContainsKey(EAreaSpecialEffectType.KillZoneEffect))
            currentSpecialEffects.Add(EAreaSpecialEffectType.KillZoneEffect, 0);

        currentSpecialEffects[EAreaSpecialEffectType.KillZoneEffect] += 1;
        SpeedDecreaseValue = speedDecreaseValue;
        UI.UiSpecialEffectsController.Get.AddEffectIndicator(EEffectType.KillZone, -1f);
    }

    public void LeftKillZone()
    {
        if (!currentSpecialEffects.ContainsKey(EAreaSpecialEffectType.KillZoneEffect))
            return;

        currentSpecialEffects[EAreaSpecialEffectType.KillZoneEffect] -= 1;
        if (currentSpecialEffects[EAreaSpecialEffectType.KillZoneEffect] <= 0)
        {
            SpeedDecreaseValue = 0;}
            UI.UiSpecialEffectsController.Get.RemoveEffectIndicator(EEffectType.KillZone);
    }
}
