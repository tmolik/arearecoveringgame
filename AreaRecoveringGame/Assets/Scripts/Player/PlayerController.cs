﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    internal Vector3 CurrentDirection = Vector3.zero;

    public float Speed = 1;

    internal bool IsOnBorder = true;

    private bool lockedTurning = false;
    private Vector3 turnAtPosition;
    private Vector3 turnDirection;

    private float speedBonus;
    private float speedBonusTime;

    [SerializeField]
    private MobileInputController mobileInputController;

    public PlayerSpecialEffectsController SpecialEffectsController;

    public bool IsInvulnerable = false;

    private Coroutine invulnerabilityCancelationCoroutine;

    [SerializeField]
    private LayerMask groundCheckLayerMask;

    [Header("Falling")]
    [SerializeField]
    private float fallSpeed = 10f;
    [SerializeField]
    private float yValueToDie = -5f;
    private bool isFalling = false;
    private bool IsDead = false;

    [SerializeField]
    private Transform modelTransform = null;
    private bool isInKillZone = false;
    private float killZoneTimeToKill = 0f;

    [SerializeField]
    private Animator playerAnimator = null;
    [SerializeField]
    private int deathAnimationsCount = 4;

    [SerializeField]
    private Transform umbrellaTransform = null;
    [SerializeField]
    private Transform offensiveBallTransform = null;

    public void CollectedInvulnerability(float duration)
    {
        if (invulnerabilityCancelationCoroutine != null)
            StopCoroutine(invulnerabilityCancelationCoroutine);

        IsInvulnerable = true;
        invulnerabilityCancelationCoroutine = StartCoroutine(CancelInvulnerability(duration));
        UI.UiSpecialEffectsController.Get.AddEffectIndicator(EEffectType.Invulnerability, duration);
    }

    private IEnumerator CancelInvulnerability(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        IsInvulnerable = false;
        UI.UiSpecialEffectsController.Get.RemoveEffectIndicator(EEffectType.Invulnerability);
    }

    // Start is called before the first frame update
    void Start()
    {
        SpecialEffectsController = new PlayerSpecialEffectsController();
        SpecialEffectsController.Player = this;
        SpecialEffectsController.UmbrellaTransform = umbrellaTransform;
        SpecialEffectsController.OffensiveBallTransform = offensiveBallTransform;
        ResetPlayer();
    }

    public void ResetPlayer()
    {
        IsOnBorder = true;
        lockedTurning = false;
        isFalling = false;
        isInKillZone = false;
        IsDead = false;
        CurrentDirection = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        BonusesUpdate();
        SpecialEffectsController.UpdateEffects();
        if (isFalling)
        {
            Falling();
        }
        else
        {
            if (IsDead)
                return;

            if (isInKillZone)
                KillZoneUpdate();

            float currentSpeed = Speed + Speed * speedBonus - Speed * SpecialEffectsController.SpeedDecreaseValue;
            transform.position += CurrentDirection * currentSpeed * Time.deltaTime;
            if (CurrentDirection != Vector3.zero)
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(CurrentDirection), Time.deltaTime * 8);
            if (lockedTurning)
            {
                if (Vector3.Distance(transform.position, turnAtPosition) < 0.1f)
                {
                    TryToSetNextDirection(turnDirection);
                    lockedTurning = false;
                }
            }
            else
            {
                CheckDirectionInput();
            }
            CheckGroundUpdate();
        }
        playerAnimator.SetBool("IsWalking", CurrentDirection != Vector3.zero);
        playerAnimator.SetFloat("WalkSpeed", SpecialEffectsController.SpeedDecreaseValue != 0 ? 1 - SpecialEffectsController.SpeedDecreaseValue : 1f);
    }

    private void KillZoneUpdate()
    {
        modelTransform.position += Vector3.down * (1.5f / killZoneTimeToKill) * Time.deltaTime;
    }

    public void EnteredKillZone(float killZoneTimeToDeath)
    {
        isInKillZone = true;
        SpecialEffectsController.EnteredKillZone(0.25f);
        killZoneTimeToKill = killZoneTimeToDeath;
    }

    public void ExitedKillZone()
    {
        isInKillZone = false;
        modelTransform.localPosition = Vector3.zero;
    }

    public void Die()
    {
        playerAnimator.CrossFade($"Death{Random.Range(0, deathAnimationsCount)}", 0.1f);
        IsDead = true;
    }

    private void Falling()
    {
        transform.position += CurrentDirection * Speed * Time.deltaTime + Vector3.down * fallSpeed * Time.deltaTime;
        if (transform.position.y <= yValueToDie)
            LevelController.Get.PlayerCaught(EPlayerCaughtSource.Fall);
    }

    private void CheckGroundUpdate()
    {
        RaycastHit raycastHit;
        if (Physics.Raycast(transform.position + Vector3.up * 0.3f, Vector3.down, out raycastHit, 2, groundCheckLayerMask, QueryTriggerInteraction.Collide))
        {
            //Debug.Log("Jest coś pod spodem :) " + raycastHit.collider.name);
        }
        else
        {
            isFalling = true;
            //Debug.Log("Nie ma nic pod spodem");
        }
    }

    private void BonusesUpdate()
    {
        if (speedBonus > 0)
        {
            speedBonusTime -= Time.deltaTime;
            if (speedBonusTime <= 0)
            {
                speedBonus = 0;
                UI.UiSpecialEffectsController.Get.RemoveEffectIndicator(EEffectType.Speed);
            }
        }
    }

    public void AddSpeedBonus(float speedBonus, float bonusLifeTime)
    {
        UI.UiSpecialEffectsController.Get.AddEffectIndicator(EEffectType.Speed, bonusLifeTime);
        this.speedBonus = speedBonus;
        speedBonusTime = bonusLifeTime;
    }

    private void CheckDirectionInput()
    {
#if UNITY_ANDROID
        Vector3 currentDirectionVector = mobileInputController.GetCurrentDirectionVector();
        if (currentDirectionVector != Vector3.zero)
            WantsToTurnFromInput(currentDirectionVector);
#else
        if (Input.GetKeyDown(KeyCode.UpArrow))
            WantsToTurnFromInput(Vector3.forward);
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            WantsToTurnFromInput(Vector3.back);
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            WantsToTurnFromInput(Vector3.left);
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            WantsToTurnFromInput(Vector3.right);
#endif

    }

    private void WantsToTurnFromInput(Vector3 newDirection)
    {
        if (!CanGoInThisDirection(newDirection))
        {
            return;
        }

        float closestCorrectX = CurrentDirection.x == 1 ? Mathf.Ceil(transform.position.x) : Mathf.Floor(transform.position.x);
        float closestCorrectZ = CurrentDirection.z == 1 ? Mathf.Ceil(transform.position.z) : Mathf.Floor(transform.position.z);
        turnAtPosition = new Vector3(closestCorrectX, transform.position.y, closestCorrectZ);
        turnDirection = newDirection;
        lockedTurning = true;
    }

    private void TryToSetNextDirection(Vector3 newDirection)
    {
        if (!CanGoInThisDirection(newDirection))
            return;

        CurrentDirection = newDirection;

        float closestCorrectX = Mathf.Round(transform.position.x);
        float closestCorrectZ = Mathf.Round(transform.position.z);
        transform.position = new Vector3(closestCorrectX, transform.position.y, closestCorrectZ);
    }

    private bool CanGoInThisDirection(Vector3 newDirection)
    {
        if (!IsOnBorder && CurrentDirection + newDirection == Vector3.zero)
            return false;

        if (CurrentDirection == newDirection)
            return false;

        if (!IsThereAnyAreaInThatDirection(transform.position, newDirection))
            return false;

        return true;
    }

    private bool IsThereAnyAreaInThatDirection(Vector3 currentAreaCenter, Vector3 direction)
    {
        Vector3 raycastOrigin = new Vector3(currentAreaCenter.x + direction.x, transform.position.y + 1, currentAreaCenter.z + direction.z);

        RaycastHit hit;
        if (Physics.Raycast(raycastOrigin, Vector3.down, out hit, 5, AreasManager.Get.AreasRaycastsLayerMask))
        {
            AreaController neightbourArea = hit.collider.GetComponent<AreaController>();
            if (neightbourArea)
            {
                return true;
            }
        }
        return false;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other == null)
            return;

        AreaController area = other.GetComponent<AreaController>();
        if (area == null)
            return;

        if (area.IsBorder || area.AreaStatus == EAreaStatus.Taken)
        {
            if (!IsOnBorder)
            {
                CurrentDirection = Vector3.zero;
                LevelController.Get.PlayerFinishedRecovering(area);
            }
            else
            {
                if (CurrentDirection != Vector3.zero && !IsThereAnyAreaInThatDirection(area.transform.position, CurrentDirection))
                {
                    Debug.Log("Zatrzymaj się do diaska");
                    if (area.IsBorder)
                        FindNewDirectionOnBorderEdge(area);
                }
            }
            area.CheckAreaEffects(this);
        }
        else
        {
            if (area.AreaStatus == EAreaStatus.TemporaryTakenByDrawing)
            {
                LevelController.Get.PlayerCaught(EPlayerCaughtSource.PlayerEnteredWindScreen);
            }
            else
            {
                IsOnBorder = false;
                area.OnEnterArea(this);
                area.CheckAreaEffects(this);
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other == null)
            return;

        AreaController area = other.GetComponent<AreaController>();
        if (area == null)
            return;

        area.OnLeftArea(this);
    }

    private void FindNewDirectionOnBorderEdge(AreaController borderEdgeArea)
    {
        foreach (AreaController area in borderEdgeArea.NeightbourAreas)
        {
            if (area.IsBorder)
            {
                Vector3 newDirection = area.transform.position - borderEdgeArea.transform.position;
                if (CanGoInThisDirection(newDirection))
                {
                    Debug.Log("Ustawiam locked ");
                    lockedTurning = true;
                    turnAtPosition = borderEdgeArea.transform.position;
                    turnDirection = newDirection;
                    return;
                }
            }
        }
    }

}
