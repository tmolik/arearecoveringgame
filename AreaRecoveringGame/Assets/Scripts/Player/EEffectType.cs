﻿
public enum EEffectType
{
    KillZone,
    SlowEffect,
    BeerSlow,
    Invulnerability,
    Speed,
    Umbrella,
    OffensiveBall
}
