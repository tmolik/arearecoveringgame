﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UiGameController : MonoBehaviour
    {
        public static UiGameController Get { get; private set; }

        public UiGameController()
        {
            Get = this;
        }

        [SerializeField]
        private TextMeshProUGUI lifesText = null;

        [SerializeField]
        private Animator caughtTextAnimator = null;

        [SerializeField]
        private RectTransform mobileInputRect = null;

        // Start is called before the first frame update
        void Start()
        {
            if (LevelController.Get != null)
                LevelController.Get.OnPlayerLifesCountChange.AddListener(UpdateLifesText);
            UpdateLifesText();
#if UNITY_ANDROID
        mobileInputRect.gameObject.SetActive(true);
#else
            mobileInputRect.gameObject.SetActive(false);
#endif
        }

        public void OnDestroy()
        {
            if (LevelController.Get != null)
                LevelController.Get.OnPlayerLifesCountChange.RemoveListener(UpdateLifesText);
        }

        public void ShowCaughtText()
        {
            caughtTextAnimator.gameObject.SetActive(true);
            caughtTextAnimator.Play("Show");
        }

        public void UpdateLifesText()
        {
            if (LevelController.Get == null || lifesText == null)
                return;

            lifesText.text = $"Lifes:{LevelController.Get.CurrentLives}";
        }

    }
}