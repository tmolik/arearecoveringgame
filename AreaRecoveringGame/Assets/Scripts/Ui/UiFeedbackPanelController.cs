﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UiFeedbackPanelController : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField inputField = null;

    public void SendEmail()
    {
        if (inputField == null)
            return;

        string email = "molik94@gmail.com";
        string subject = MyEscapeURL("ARG Feedback");
        string body = MyEscapeURL(inputField.text);
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}
