﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class UiSpecialEffectsController : MonoBehaviour
    {
        public static UiSpecialEffectsController Get { get; private set; }

        public UiSpecialEffectsController()
        {
            Get = this;
        }

        private List<UiEffectIndicator> EffectsIndicators = new List<UiEffectIndicator>();

        [SerializeField]
        private List<EffectSpriteReference> spritesReference = new List<EffectSpriteReference>();

        [SerializeField]
        private RectTransform effectsRect = null;

        public void AddEffectIndicator(EEffectType effectType, float time)
        {
            for (int i = 0; i <= EffectsIndicators.Count - 1; i++)
            {
                if (EffectsIndicators[i].EffectType == effectType)
                {
                    EffectsIndicators[i].Setup(effectType, GetEffectSprite(effectType), time);
                    return;
                }
            }

            UiEffectIndicator indicator = GetNextFreeIndicator();
            indicator.Setup(effectType, GetEffectSprite(effectType), time);
            indicator.transform.SetParent(effectsRect);
            indicator.gameObject.SetActive(true);
            EffectsIndicators.Add(indicator);
        }

        public void RemoveEffectIndicator(EEffectType effectType)
        {
            for (int i = 0; i <= EffectsIndicators.Count - 1; i++)
            {
                if (EffectsIndicators[i].EffectType == effectType)
                {
                    EffectsIndicators[i].gameObject.SetActive(false);
                    EffectsIndicators.RemoveAt(i);
                    return;
                }
            }
        }

        private UiEffectIndicator GetNextFreeIndicator()
        {
            GameObject effectGameobject = ObjectPooler.Get.GetPooledObject(EPoolables.UiEffectIndicator);
            return effectGameobject.GetComponent<UiEffectIndicator>();
        }

        private Sprite GetEffectSprite(EEffectType effectType)
        {
            for (int i = 0; i <= spritesReference.Count - 1; i++)
            {
                if (spritesReference[i].EffectType == effectType)
                    return spritesReference[i].EffectSprite;
            }
            return null;

        }
    }
}

[System.Serializable]
public class EffectSpriteReference
{
    public EEffectType EffectType;
    public Sprite EffectSprite;
}
