﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class UiPauseMenu : MonoBehaviour
    {
        public void OnEnable()
        {
            Time.timeScale = 0;
        }

        public void OnDisable()
        {
            Time.timeScale = 1;
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                OnClickContinue();
        }

        public void OnClickContinue()
        {
            gameObject.SetActive(false);
        }

        public void OnClickGoToBeach()
        {
            if (ScenesManager.Get != null)
                ScenesManager.Get.LoadScene("LevelSelect");
        }
    }
}