﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UiSummaryPanelController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI titleText = null;

    [SerializeField]
    private RectTransform repeatButtonRect = null;
    [SerializeField]
    private RectTransform nextLevelButtonRect = null;

    public void Show()
    {
        gameObject.SetActive(true);
        if (GameSpeedManager.Get)
            GameSpeedManager.Get.PauseGame();
    }

    public void Hide()
    {
        if (GameSpeedManager.Get)
            GameSpeedManager.Get.UnpauseGame();
        gameObject.SetActive(false);
    }

    public void Setup(bool win)
    {
        titleText.text = win ? "Win" : "Lose";
        repeatButtonRect.gameObject.SetActive(!win);
        nextLevelButtonRect.gameObject.SetActive(win);
    }

    public void OnClickedRepeat()
    {
        Hide();
        if (Levels.LevelsManager.Get != null)
            Levels.LevelsManager.Get.ResetLevel();
    }

    public void OnClickedBackToMap()
    {
        Hide();
        if (ScenesManager.Get != null)
            ScenesManager.Get.LoadScene("LevelSelect");
    }

    public void OnClickedNextLevel()
    {
        Hide();
        if (Levels.LevelsManager.Get != null)
            Levels.LevelsManager.Get.LoadNextLevel();
    }
}
