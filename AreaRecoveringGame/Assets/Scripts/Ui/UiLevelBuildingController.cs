﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace LevelBuilding
{
    public class UiLevelBuildingController : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI currentBuilderText;

        [SerializeField]
        private LevelBuilderController levelBuilder;

        public void Update()
        {
            currentBuilderText.text = levelBuilder.MainEmptyBuilder == null ? "" : levelBuilder.MainEmptyBuilder.gameObject.name;
        }
    }
}