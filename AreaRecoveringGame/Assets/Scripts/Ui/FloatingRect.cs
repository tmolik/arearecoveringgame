﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingRect : MonoBehaviour
{

    private Vector3 _startingPosition;

    private RectTransform _rectTransform = null;

    private float xRandomizer = 0;
    private float yRandomizer = 0;

    private float xTimeRandomizer = 0;
    private float yTimeRandomizer = 0;

    // Start is called before the first frame update
    void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _startingPosition = _rectTransform.position;
        xRandomizer = Random.Range(-50, 50);
        yRandomizer = Random.Range(-20, 20);

        xTimeRandomizer = Random.Range(1f, 3f);
        yTimeRandomizer = Random.Range(1f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (_rectTransform == null)
            return;

        _rectTransform.position += new Vector3(Mathf.Sin(Time.time * xTimeRandomizer) * xRandomizer, Mathf.Sin(Time.time * yTimeRandomizer) * yRandomizer)  * Time.deltaTime;
    }
}
