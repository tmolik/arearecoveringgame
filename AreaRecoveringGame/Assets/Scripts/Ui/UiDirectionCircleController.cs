﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiDirectionCircleController : MonoBehaviour
{
    [SerializeField]
    private RectTransform indicatorRect = null;

    [SerializeField]
    private float maxDistance = 50;

    private RectTransform rectTransform;

    private Vector3 directionVector;

    public Vector3 GetDirectionVector()
    {
        return directionVector;
    }

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (indicatorRect == null)
            return;

#if UNITY_ANDROID
        AndroidUpdate();
#elif UNITY_EDITOR
        EditorUpdate();
#endif
    }

    private void EditorUpdate()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            float distance = Vector3.Distance(Input.mousePosition, rectTransform.position);
            if (distance > maxDistance)
            {
                Vector3 direction = (Input.mousePosition - rectTransform.position).normalized;
                indicatorRect.position = rectTransform.position + direction * maxDistance;
            }
            else
            {
                indicatorRect.position = Input.mousePosition;
            }

            CheckAngle();
        }
        else
        {
            directionVector = Vector3.zero;
        }
    }

    private void AndroidUpdate()
    {
        if (Input.touchCount == 0)
            return;

        if (Input.touches[0].phase == TouchPhase.Stationary || Input.touches[0].phase == TouchPhase.Moved)
        {
            float distance = Vector3.Distance(Input.touches[0].position, rectTransform.position);
            if (distance > maxDistance)
            {
                Vector3 inputVector3 = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 0);
                Vector3 direction = (inputVector3 - rectTransform.position).normalized;
                indicatorRect.position = rectTransform.position + direction * maxDistance;
            }
            else
            {
                Vector3 inputVector3 = new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 0);
                indicatorRect.position = inputVector3;
            }

            CheckAngle();
        }
        else
        {
            directionVector = Vector3.zero;
        }
    }

    private void CheckAngle()
    {
        Vector3 direction = (Input.mousePosition - rectTransform.position).normalized;
        float angleRight = Vector3.Angle(Vector3.right, direction);
        if (angleRight > 0 && angleRight < 45)
        {
            directionVector = Vector3.right;
        }
        else if (angleRight > 135 && angleRight < 180)
        {
            directionVector = Vector3.left;
        }

        float angleUp = Vector3.Angle(Vector3.up, direction);
        if (angleUp > 0 && angleUp < 45)
        {
            directionVector = Vector3.forward;
        }
        else if (angleUp > 135 && angleUp < 180)
        {
            directionVector = Vector3.back;
        }
    }
}
