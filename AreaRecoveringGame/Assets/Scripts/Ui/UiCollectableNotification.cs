﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cameras;
using Levels;
using TMPro;

namespace UI
{
    public class UiCollectableNotification : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI titleText = null;
        [SerializeField]
        private TextMeshProUGUI subtitleText = null;


        [SerializeField]
        private float lifeTime = 10f;

        private float helperTimer = 0;

        [SerializeField]
        private Vector3 collectablePosition;

        private Camera mainCamera;
        private RectTransform rectTransform;

        // Start is called before the first frame update
        void Start()
        {
            mainCamera = LevelController.Get.FollowingCamera.CameraComponent;
            rectTransform = GetComponent<RectTransform>();
        }

        // Update is called once per frame
        void Update()
        {
            rectTransform.position = mainCamera.WorldToScreenPoint(collectablePosition + Vector3.up);
            helperTimer += Time.deltaTime;
            if(helperTimer>=lifeTime)
            {
                gameObject.SetActive(false);
            }
        }


        public void SetupTarget(Transform collectableTransform)
        {
            collectablePosition = collectableTransform.position + Vector3.up;
            helperTimer = 0;
            lifeTime = 2.2f; //hard coded bad tomek
        }

        public void SetupValues(CollectableNotificationSetup setup)
        {
            if (setup == null)
                return;

            subtitleText.text = setup.SubtitleText;
            titleText.text = setup.TitleText;
            titleText.color = setup.TitleColor;
        }
    }
}