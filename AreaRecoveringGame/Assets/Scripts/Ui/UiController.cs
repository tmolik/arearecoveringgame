﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class UiController : MonoBehaviour
    {
        public static UiController Get { get; private set; }

        public UiController()
        {
            Get = this;
        }

        public UiSummaryPanelController SummaryPanel;
        public UiCollectablesNotificationsManager CollectablesNotficationsManager;
        public UiPauseMenu PauseMenu;

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                PauseMenu.gameObject.SetActive(true);
        }
    }
}
