﻿using Collectables;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UI
{
    public class UiCollectablesNotificationsManager : MonoBehaviour
    {
        [SerializeField]
        private List<CollectableNotificationSetup> notificationsSetups = new List<CollectableNotificationSetup>();

        public void ShowNotification(CollectableObjectController collectable)
        {
            GameObject collectableNotificationGameObject = ObjectPooler.Get.GetPooledObject(EPoolables.UiCollectableNotification);
            UiCollectableNotification notification = collectableNotificationGameObject.GetComponent<UiCollectableNotification>();
            notification.SetupTarget(collectable.transform);
            notification.SetupValues(notificationsSetups.FirstOrDefault(n => n.CollectableType == collectable.CollectableType));
            notification.gameObject.SetActive(true);
            notification.transform.SetParent(transform);
        }
    }


    
}

[System.Serializable]
public class CollectableNotificationSetup
{
    public ECollectableType CollectableType;
    public string TitleText;
    public string SubtitleText;
    public Color TitleColor;
}