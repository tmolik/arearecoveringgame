﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UiEffectIndicator : MonoBehaviour
    {
        [System.NonSerialized]
        public EEffectType EffectType;

        [SerializeField]
        private Image effectImage;

        [SerializeField]
        private TextMeshProUGUI timeText;

        public float TimeLeft = 0f;

        public void Setup(EEffectType effectType, Sprite effectIcon, float timeLeft)
        {
            EffectType = effectType;
            effectImage.sprite = effectIcon;
            timeText.text = timeLeft >= 0 ? $"{timeLeft}" : "";
            TimeLeft = timeLeft;
        }

        public void Update()
        {
            if (TimeLeft == -1f)
            {
                timeText.text = "";
                return;
            }
            else
            {
                TimeLeft -= Time.deltaTime;
                timeText.text = Mathf.Max(0, TimeLeft).ToString("F1");
            }

        }

    }
}