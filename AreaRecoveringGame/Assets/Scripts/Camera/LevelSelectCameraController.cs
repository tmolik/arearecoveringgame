﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cameras
{

    public class LevelSelectCameraController : MonoBehaviour
    {
        [SerializeField]
        private Transform startTransform = null;
        [SerializeField]
        private Transform endTransform = null;

        [SerializeField]
        private float moveSpeed = 3f;

        private Vector3 dragOrigin;
        private Vector2 androidDragOrigin;

        private Vector3 inertia;
        private float inertiaShutdownMultiplier = 0.98f;

        // Start is called before the first frame update
        void Start()
        {
            if (startTransform == null)
                return;

            transform.position = startTransform.position;
        }


        public void ResetToStartingPosition()
        {
            transform.position = startTransform.position;
            inertia = Vector3.zero;
        }

        // Update is called once per frame
        void Update()
        {
            if (startTransform == null || endTransform == null)
                return;
#if UNITY_ANDROID
            AndroidInputUpdate();
#else
            EditorInputUpdate();
#endif
        }

        private void EditorInputUpdate()
        {
            if (Input.GetMouseButtonDown(0))
            {
                dragOrigin = Input.mousePosition;
                return;
            }

            if (!Input.GetMouseButton(0))
            {
                InertiaMove();
                return;
            }

            Vector3 dragDiff = Input.mousePosition - dragOrigin;
            dragDiff *= -1;

            Vector3 newPosition = transform.position + new Vector3(0, 0, dragDiff.y) * moveSpeed * Time.deltaTime;
            newPosition.z = Mathf.Clamp(newPosition.z, startTransform.position.z, endTransform.position.z);

            inertia = newPosition - transform.position;
            transform.position = newPosition;
        }

        private void AndroidInputUpdate()
        {
            if (Input.touchCount == 0)
            {
                InertiaMove();
                return;
            }

            if (Input.touches[0].phase == TouchPhase.Began)
            {
                androidDragOrigin = Input.touches[0].position;
                return;
            }

            if (Input.touches[0].phase == TouchPhase.Moved)
            {
                Vector2 dragDiff = Input.touches[0].position - androidDragOrigin;
                dragDiff *= -1;

                Vector3 newPosition = transform.position + new Vector3(0, 0, dragDiff.y) * moveSpeed * Time.deltaTime;
                newPosition.z = Mathf.Clamp(newPosition.z, startTransform.position.z, endTransform.position.z);
                transform.position = newPosition;
            }
        }

        private void InertiaMove()
        {
            if (inertia == Vector3.zero)
                return;

            Vector3 inertiaPosition = transform.position + inertia;
            inertiaPosition.z = Mathf.Clamp(inertiaPosition.z, startTransform.position.z, endTransform.position.z);

            transform.position = inertiaPosition;
            inertia.z *= inertiaShutdownMultiplier;
        }
    }
}