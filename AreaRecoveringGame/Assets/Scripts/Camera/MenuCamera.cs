﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cameras
{
    public class MenuCamera : MonoBehaviour
    {
        public bool IsMoving = false;

        [SerializeField]
        private Transform _menuCameraPlace = null;

        [SerializeField]
        private MenuManager _menuManager = null;

        [SerializeField]
        private LevelSelectCameraController _levelSelectCamera = null;

        [SerializeField]
        private float _changePositionTime = 1f;

        private Vector3 _startingPosition;
        private Quaternion _startingRotation;

        private float _timer = 0;

        private Coroutine currentCoroutine = null;

        public void GoToLevelSelect()
        {
            if (currentCoroutine != null)
                return;
            currentCoroutine = StartCoroutine(GoToPositionAndRotation(_levelSelectCamera.transform.position, _levelSelectCamera.transform.rotation, EnableLevelSelectCamera));
        }

        public void GoToMenu()
        {
            if (currentCoroutine != null)
                return;

            gameObject.SetActive(true);
            transform.position = _levelSelectCamera.transform.position;
            transform.rotation = _levelSelectCamera.transform.rotation;
            _levelSelectCamera.gameObject.SetActive(false);
            _levelSelectCamera.ResetToStartingPosition();
            currentCoroutine = StartCoroutine(GoToPositionAndRotation(_menuCameraPlace.position, _menuCameraPlace.rotation, _menuManager.CameraBackOnPosition));
        }

        private IEnumerator GoToPositionAndRotation(Vector3 position, Quaternion rotation, Action onFinished = null)
        {
            IsMoving = true;
            _startingPosition = transform.position;
            _startingRotation = transform.rotation;
            _timer = 0;

            while (_timer <= _changePositionTime)
            {
                _timer += Time.deltaTime;
                transform.position = Vector3.Lerp(_startingPosition, position, _timer / _changePositionTime);
                transform.rotation = Quaternion.Lerp(_startingRotation, rotation, _timer / _changePositionTime);
                yield return null;
            }
            onFinished.Invoke();
            IsMoving = false;
            currentCoroutine = null;
        }

        private void EnableLevelSelectCamera()
        {
            _levelSelectCamera.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}