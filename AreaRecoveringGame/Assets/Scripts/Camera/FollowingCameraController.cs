﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cameras
{
    public class FollowingCameraController : MonoBehaviour
    {
        private Transform targetTransform = null;

        [SerializeField]
        private float movementSmoothness = 5f;

        [SerializeField]
        private Vector3 positionOffset = Vector3.zero;

        internal Camera CameraComponent;

        private float shake = 0;
        private float shakeAmount = 0.7f;
        private float decreaseFactor = 1.0f;

        public void Awake()
        {
            CameraComponent = GetComponent<Camera>();
            CameraComponent.orthographic = false;
        }


        // Update is called once per frame
        void Update()
        {
            if (targetTransform == null)
            {
                targetTransform = Levels.LevelController.Get.GetPlayer().transform;
                return;
            }

            transform.position = Vector3.Lerp(transform.position, targetTransform.position + positionOffset, movementSmoothness * Time.deltaTime);
            transform.position += ShakeUpdate();
        }

        private Vector3 ShakeUpdate()
        {
            if (shake > 0)
            {
                shake -= Time.unscaledDeltaTime * decreaseFactor;
                return Random.insideUnitSphere * shakeAmount;
            }
            else
            {
                shake = 0.0f;
                return Vector3.zero;
            }
        }

        public void Shake(float shakeTime, float shakeAmount)
        {
            shake = shakeTime;
            this.shakeAmount = shakeAmount;
        }

        public void SetupTarget(Transform target)
        {
            targetTransform = target;
            transform.position = targetTransform.position + positionOffset;
            transform.LookAt(targetTransform);
        }

    }
}