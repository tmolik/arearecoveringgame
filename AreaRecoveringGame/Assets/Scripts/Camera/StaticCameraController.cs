﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cameras
{
    public class StaticCameraController : MonoBehaviour
    {
        [SerializeField]
        private AreasManager areasManager = null;

        [ContextMenu("Center camera")]
        public void SetupCameraToBeInCenter()
        {
            if (areasManager == null)
                return;

            Vector3 centerPositionOfAreas = areasManager.GetAreasCenterPosition();
            transform.position = new Vector3(centerPositionOfAreas.x, transform.position.y, centerPositionOfAreas.z);
        }
    }
}