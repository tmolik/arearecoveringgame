﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public enum ECollectableType
    {
        Speed,
        SlowTime,
        Life,
        Invulnerability,
        Umbrella,
        OffensiveBall
    }
}