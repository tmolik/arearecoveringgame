﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffensiveBallController : MonoBehaviour
{
    [SerializeField]
    private float flySpeed = 10f;

    private Rigidbody ballRigidbody;
    private Collider ballCollider;


    public void OnDisable()
    {
        if (ballRigidbody)
            ballRigidbody.velocity = Vector3.zero;
    }


    public void ThrowedAt(Vector3 direction)
    {
        if (ballRigidbody == null)
            ballRigidbody = GetComponent<Rigidbody>();
        if (ballCollider == null)
            ballCollider = GetComponent<Collider>();
        ballCollider.enabled = false;
        ballRigidbody.isKinematic = false;
        StartCoroutine(AddForceAfterFrame(direction * flySpeed));
    }

    private IEnumerator AddForceAfterFrame(Vector3 force)
    {
        yield return new WaitForEndOfFrame();
        ballRigidbody.AddForce(force, ForceMode.Impulse);
        yield return new WaitForSeconds(0.1f);
        ballCollider.enabled = true;
    }


    void Start()
    {
        ballRigidbody = GetComponent<Rigidbody>();
    }

    public void OnTriggerEnter(Collider other)
    {
        IHittable hittable = other.GetComponent<IHittable>();
        if (hittable != null)
        {
            hittable.Hit(5f);
            gameObject.SetActive(false);
        }
    }

}
