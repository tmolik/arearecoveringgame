﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserController : MonoBehaviour
{
    [SerializeField]
    private Transform objectToActivate = null;

    [SerializeField]
    private float activatedTime = 1f;
    [SerializeField]
    private float deactivatedTime = 2f;

    private float helperTimer = 0;
    private bool isObjectActive = false;

    [Header("Towers setup")]
    [SerializeField]
    private Transform leftTowerTransform = null;
    [SerializeField]
    private Transform rightTowerTransform = null;
    [SerializeField]
    private Transform laserTransform = null;

    [Range(2, 100)]
    [SerializeField]
    private float distanceBetweenTowers = 22f;

    // Update is called once per frame
    void Update()
    {
        helperTimer += Time.deltaTime;
        if (helperTimer >= (isObjectActive ? activatedTime : deactivatedTime))
        {
            isObjectActive = !isObjectActive;
            objectToActivate.gameObject.SetActive(isObjectActive);
            helperTimer = 0;
        }
    }

    public void OnValidate()
    {
        if (leftTowerTransform == null || rightTowerTransform == null || laserTransform == null)
            return;

        leftTowerTransform.localPosition = new Vector3(-1 * distanceBetweenTowers / 2f, leftTowerTransform.localPosition.y, leftTowerTransform.localPosition.z);
        rightTowerTransform.localPosition = new Vector3(distanceBetweenTowers / 2f, leftTowerTransform.localPosition.y, leftTowerTransform.localPosition.z);
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, distanceBetweenTowers / 2f, laserTransform.localScale.z);
    }
}
