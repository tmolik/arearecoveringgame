﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class CollectableTransportController : MonoBehaviour
    {
        [SerializeField]
        private float fallSpeed = 10f;

        [HideInInspector]
        public ECollectableType CollectableType;

        [SerializeField]
        private GameObject FracturedCratePrefab;


        private bool isFalling = false;
        private Vector3 fallPosition;
        private float helperTimer = 0;

        // Update is called once per frame
        void Update()
        {
            if (isFalling)
            {
                transform.position += Vector3.down * fallSpeed * Time.deltaTime;
                helperTimer -= Time.deltaTime;
                if (helperTimer <= 0)
                {
                    Fallen();
                }
            }
        }

        public void Fall()
        {
            RaycastHit raycastHit;
            if (Physics.Raycast(transform.position + Vector3.down, Vector3.down * 10, out raycastHit, 100))
            {
                fallPosition = raycastHit.point;
                isFalling = true;
                helperTimer = Vector3.Distance(transform.position, fallPosition) / fallSpeed;
            }
        }

        private void Fallen()
        {
            EPoolables poolableCollectable = EPoolables.SpeedCollectable;
            switch (CollectableType)
            {
                case ECollectableType.Speed:
                    poolableCollectable = EPoolables.SpeedCollectable;
                    break;
                case ECollectableType.SlowTime:
                    poolableCollectable = EPoolables.SlowTimeCollectable;
                    break;
                case ECollectableType.Life:
                    poolableCollectable = EPoolables.LifeCollectable;
                    break;
                case ECollectableType.Invulnerability:
                    poolableCollectable = EPoolables.InvulnerabilityCollectable;
                    break;
                case ECollectableType.Umbrella:
                    poolableCollectable = EPoolables.UmbrellaCollectable;
                    break;
                case ECollectableType.OffensiveBall:
                    poolableCollectable = EPoolables.OffensiveBallCollectable;
                    break;
            }
            GameObject collectableGameObject = ObjectPooler.Get.GetPooledObject(poolableCollectable);
            if (collectableGameObject != null)
            {
                collectableGameObject.transform.position = fallPosition;
                collectableGameObject.gameObject.SetActive(true);
            }

            isFalling = false;
            CreatePuffParticle();
            Instantiate(FracturedCratePrefab, transform.position, transform.rotation);
            gameObject.SetActive(false);
    }

    private void CreatePuffParticle()
    {
        GameObject puffGameObject = ObjectPooler.Get.GetPooledObject(EPoolables.SmokePuff);
        puffGameObject.transform.position = fallPosition;
        puffGameObject.SetActive(true);
    }
}
}