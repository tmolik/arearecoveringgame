﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class SlowTimeCollectableController : CollectableObjectController
    {
        [SerializeField]
        private float newTimeScale = 0.7f;

        [SerializeField]
        private float duration = 3f;

        public override void OnCollected(PlayerController player)
        {
            if (GameSpeedManager.Get != null)
                GameSpeedManager.Get.ChangeTime(newTimeScale, duration);

            base.OnCollected(player);
        }
    }
}