﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class UmbrellaCollectableController : CollectableObjectController
    {
        [SerializeField]
        private float umbrellaTime = 5f;

        public override void OnCollected(PlayerController player)
        {
            player.SpecialEffectsController.EquipUmbrella(umbrellaTime);
            base.OnCollected(player);
        }
    }
}