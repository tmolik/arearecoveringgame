﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class OffensiveBallCollectableController : CollectableObjectController
    {
        public override void OnCollected(PlayerController player)
        {
            player.SpecialEffectsController.EquipBall();
            base.OnCollected(player);
        }
    }
}