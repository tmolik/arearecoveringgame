﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class CollectableObjectController : MonoBehaviour
    {
        public ECollectableType CollectableType;

        [SerializeField]
        private float rotationRate = 30;

        [SerializeField]
        private float yMovementRange = 2;

        [SerializeField]
        private float lifeTime = 5f;
        [SerializeField]
        private bool deactivateAfterLifeTime = true;

        private Vector3 startPosition;

        [SerializeField]
        private float normalScaleFactor = 1;

        [SerializeField]
        private float shrinkingTime = 0.5f;
        private float helperShinkingTimer = 0;

        private bool isShrinking = false;

        public void OnEnable()
        {
            startPosition = transform.position;
            transform.localScale = Vector3.one * normalScaleFactor;
            StopAllCoroutines();
            if (deactivateAfterLifeTime)
                StartCoroutine(DisableAfterLifetimeEnded());
        }

        // Update is called once per frame
        void Update()
        {
            if (isShrinking)
            {
                helperShinkingTimer += Time.deltaTime;
                transform.localScale = Vector3.one * Mathf.Lerp(normalScaleFactor, 0, helperShinkingTimer / shrinkingTime);

                if (helperShinkingTimer > shrinkingTime)
                {
                    isShrinking = false;
                    gameObject.SetActive(false);
                }
            }

            //Some nice movement here
            float yMovement = Mathf.Sin(Time.time * 2) * yMovementRange * Time.deltaTime;
            transform.position = startPosition + Vector3.up * yMovement;
            transform.Rotate(new Vector3(0, rotationRate * Time.deltaTime, 0));
        }

        public void OnTriggerEnter(Collider other)
        {
            PlayerController player = other.GetComponent<PlayerController>();
            if (player != null)
            {
                OnCollected(player);
                isShrinking = true;
                helperShinkingTimer = 0;
                ShowNotification();
            }
        }

        private void ShowNotification()
        {
            UI.UiController.Get.CollectablesNotficationsManager.ShowNotification(this);
        }

        public virtual void OnCollected(PlayerController player)
        {
        }

        private IEnumerator DisableAfterLifetimeEnded()
        {
            yield return new WaitForSeconds(lifeTime);
            isShrinking = true;
            helperShinkingTimer = 0;
        }
    }
}