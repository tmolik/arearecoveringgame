﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class SpeedCollectableController : CollectableObjectController
    {
        [SerializeField]
        private float speedBonus = 10f;
        [SerializeField]
        private float speedBonusDuration = 5f;

        public override void OnCollected(PlayerController player)
        {
            player.AddSpeedBonus(speedBonus, speedBonusDuration);
            base.OnCollected(player);
        }
    }
}