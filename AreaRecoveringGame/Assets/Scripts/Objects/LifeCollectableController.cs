﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class LifeCollectableController : CollectableObjectController
    {
        public override void OnCollected(PlayerController player)
        {
            if (LevelController.Get != null)
                LevelController.Get.CollectedLife();
            base.OnCollected(player);
        }
    }
}
