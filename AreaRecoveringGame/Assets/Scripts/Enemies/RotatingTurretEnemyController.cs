﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemies
{
    public class RotatingTurretEnemyController : TurretEnemyController
    {
        [SerializeField]
        public ETurretRotationType RotationType;

        [SerializeField]
        private float rotationSpeed = 30;

        private Transform playerTransform;

        [SerializeField]
        private List<Vector3> eulerAnglesTargets = new List<Vector3>();
        private Vector3 currentAngleTarget;


        public void Start()
        {
            playerTransform = LevelController.Get.GetPlayer().transform;
            if (RotationType == ETurretRotationType.RotateDefined && eulerAnglesTargets.Count > 0)
                currentAngleTarget = eulerAnglesTargets[0];
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            RotationUpdate();
        }

        private void RotationUpdate()
        {
            if (RotationType == ETurretRotationType.RotateToPlayer)
            {
                if (playerTransform == null)
                    return;

                Vector3 directionToPlayer = playerTransform.position - transform.position;
                directionToPlayer.y = 0;

                Quaternion targetRotation = Quaternion.LookRotation(directionToPlayer, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            }
            else
            {
                Quaternion targetRotation = Quaternion.Euler(currentAngleTarget);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                if (Vector3.Distance(transform.rotation.eulerAngles, currentAngleTarget) < 0.5f)
                    SetNextAngleTarget();
            }
        }

        private void SetNextAngleTarget()
        {
            int indexOfCurrentAngleTarget = eulerAnglesTargets.IndexOf(currentAngleTarget);
            int nextIndex = (indexOfCurrentAngleTarget + 1) % eulerAnglesTargets.Count;
            currentAngleTarget = eulerAnglesTargets[nextIndex];
        }

        [ContextMenu("Add current rotation as target rotation")]
        public void AddCurrentRotationToRotationTargets()
        {
            if (eulerAnglesTargets == null)
                eulerAnglesTargets = new List<Vector3>();
            eulerAnglesTargets.Add(transform.rotation.eulerAngles);
        }

        public void ClearDefinedRotations()
        {
            eulerAnglesTargets.Clear();
        }
    }

    public enum ETurretRotationType
    {
        RotateToPlayer,
        RotateDefined
    }
}