﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBulletController : DeadlyTriggerController
{
    [SerializeField]
    private float moveSpeed = 4f;

    [SerializeField]
    private Collider turretCollider = null;

    private Vector3 direction;

    public void SetDirection(Vector3 turretDirection)
    {
        direction = turretDirection;
    }

    public void OnEnable()
    {
        turretCollider.enabled = false;
        StartCoroutine(EnableCollider());
    }

    private IEnumerator EnableCollider()
    {
        yield return new WaitForSeconds(1f);
        turretCollider.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += direction * moveSpeed * Time.deltaTime;
    }

    public override void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player != null)
        {
            if(player.SpecialEffectsController.HasBouncingUmbrella)
            {
                direction *= -1;
                return;
            }
        }

        IHittable shockable = other.GetComponent<IHittable>();
        if (shockable != null)
            shockable.Hit(5f);

        base.OnTriggerEnter(other);
        CreateHitEffect();
        turretCollider.enabled = false;
        StopAllCoroutines();
        gameObject.SetActive(false);
    }

    private void CreateHitEffect()
    {
        GameObject hitGameObject = ObjectPooler.Get.GetPooledObject(EPoolables.BulletHit);
        hitGameObject.transform.position = transform.position;
        hitGameObject.SetActive(true);
    }
}
