﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemies
{
    public class TurretEnemyController : MonoBehaviour, IHittable
    {
        [SerializeField]
        private Transform bulletCreateTransform = null;

        [SerializeField]
        private float TimeBetweenShots = 1f;

        private float shotTimer = 0;
        protected bool isShooting = false;

        private bool isInShock = false;
        private float shockTimer = 0;

        [SerializeField]
        private Animator enemyAnimator = null;

        [SerializeField]
        private GameObject shockEffect = null;

        // Start is called before the first frame update
        void Start()
        {
            shotTimer = Random.Range(0f, TimeBetweenShots);
            isShooting = false;
        }

        // Update is called once per frame
        public virtual void Update()
        {
            if (isShooting)
                return;

            if (isInShock)
            {
                shockTimer -= Time.deltaTime;
                if (shockTimer <= 0)
                {
                    isInShock = false;
                    shockEffect.SetActive(false);
                }
                return;
            }

            shotTimer += Time.deltaTime;
            if (shotTimer >= TimeBetweenShots)
            {
                isShooting = true;
                StartCoroutine(Shoot());
            }
        }

        public void Hit(float shockTime)
        {
            shockTimer = shockTime;
            isInShock = true;
            enemyAnimator.Play("Idle");
            shockEffect.SetActive(true);
        }

        public void ShootBullet()
        {
            if (ObjectPooler.Get == null)
                return;

            EPoolables bulletPoolable = Random.Range(0, 101) > 50 ? EPoolables.TurretBullet : EPoolables.TurretBullet2;

            GameObject bulletGameObject = ObjectPooler.Get.GetPooledObject(bulletPoolable);
            if (bulletGameObject == null)
            {
                Debug.Log("Bullet jest nullem shoot");
                return;
            }

            bulletGameObject.transform.position = bulletCreateTransform.position;
            bulletGameObject.transform.rotation = transform.rotation;
            bulletGameObject.SetActive(true);

            TurretBulletController bullet = bulletGameObject.GetComponent<TurretBulletController>();
            bullet.SetDirection(transform.forward);

            shotTimer = 0;
        }

        private IEnumerator Shoot()
        {
            shotTimer = 0;
            enemyAnimator.SetTrigger("Shoot");

            //IT WOULD BE MUCH BETTER TO USE ANIMATION EVENT BUT CANT SET EVENT IN ANIMATION CLIP FROM MIXAMO SO USING DELAYS 
            yield return new WaitForSecondsRealtime(0.35f);
            ShootBullet();
            yield return new WaitForSecondsRealtime(1.25f);
            isShooting = false;
        }
    }
}