﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemies
{

    public class GrenadeThrowingEnemyController : WalkingEnemyController
    {
        [SerializeField]
        private float grenadeThrowInterval = 5f;

        private float grenadeHelperTimer = 0;

        private bool isThrowingGrenade = false;

        private AreaController areaToThrowGrenadeAt;

        [SerializeField]
        private Transform grenadeCreationTransform = null;

        [SerializeField]
        private Animator cornTextAnimator = null;

        public override void Start()
        {
            grenadeHelperTimer = Random.Range(0, grenadeThrowInterval);
            base.Start();
        }


        // Update is called once per frame
        public override void Update()
        {
            if (!isThrowingGrenade)
            {
                grenadeHelperTimer += Time.deltaTime;
                if (grenadeHelperTimer >= grenadeThrowInterval)
                {
                    grenadeHelperTimer = 0;
                    isThrowingGrenade = true;
                    StartCoroutine(ThrowGrenadeRoutine());
                }
            }
            else
            {
                Vector3 direction = areaToThrowGrenadeAt.transform.position - transform.position;
                direction.Normalize();
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * 5);
            }
            base.Update();
        }

        public override void ReachedDestination()
        {
            base.ReachedDestination();
        }


        private void ThrowGrenade()
        {
            if (Levels.LevelController.Get == null || AreasManager.Get == null || ObjectPooler.Get == null)
                return;

            if (areaToThrowGrenadeAt == null)
                return;

            GrenadeController grenade = ObjectPooler.Get.GetPooledObject(EPoolables.Grenade).GetComponent<GrenadeController>();
            grenade.transform.position = grenadeCreationTransform.position;
            grenade.SetDestinationPosition(areaToThrowGrenadeAt.transform.position);
            grenade.gameObject.SetActive(true);

        }

        private AreaController FindAreaAtPosition(Vector3 position)
        {
            RaycastHit hit;
            if (Physics.Raycast(position, Vector3.down, out hit, 5, AreasManager.Get.AreasRaycastsLayerMask))
            {
                AreaController hitArea = hit.collider.GetComponent<AreaController>();
                if (hitArea)
                {
                    return hitArea;
                }
            }
            return null;
        }

        private IEnumerator ThrowGrenadeRoutine()
        {
            Transform playerTransfom = Levels.LevelController.Get.GetPlayer().transform;
            //areaToThrowGrenadeAt = FindAreaAtPosition(playerTransfom.position + playerTransfom.forward * Random.Range(2f, 5f));
            if (areaToThrowGrenadeAt == null)
                areaToThrowGrenadeAt = AreasManager.Get.GetAreasInDistanceRange(transform.position, 4,10).GetRandom();

            cornTextAnimator.Play("CornTextAnimation");
            navAgent.speed = 0;
            enemyAnimator.SetTrigger("Throw");
            yield return new WaitForSeconds(0.55f);
            ThrowGrenade();
            yield return new WaitForSeconds(1f);
            isThrowingGrenade = false;
            navAgent.speed = moveSpeed;
            areaToThrowGrenadeAt = null;
            ForceRotation();
        }
    }
}