﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Enemies
{
    public class WalkingEnemyController : MonoBehaviour, IHittable
    {
        [SerializeField]
        private EWalkingRouteType RouteType = EWalkingRouteType.RandomPositions;

        private Vector3 currentDestination;
        private Vector3 currentDirection;

        [SerializeField]
        protected float moveSpeed = 2f;
        [SerializeField]
        private float timeToSpendAtPosition = 1f;

        [Header("Defined positions")]
        private List<Transform> definedPositions = new List<Transform>();
        private Transform currentDestinationPosition;

        protected NavMeshAgent navAgent;
        [SerializeField]
        protected Animator enemyAnimator;

        private bool isRotating = false;
        private bool IsWaitingAtPosition = false;
        private float helperTimer = 0;

        public virtual void Start()
        {
            if (enemyAnimator == null)
                enemyAnimator = GetComponent<Animator>();
            navAgent = GetComponent<NavMeshAgent>();
            navAgent.speed = moveSpeed;
            navAgent.updateRotation = false;
            if (RouteType == EWalkingRouteType.DefinedPositions)
                currentDestinationPosition = definedPositions.Last();
            SelectNewDestination();
            isRotating = true;
        }


        // Update is called once per frame
        public virtual void Update()
        {
            if (IsWaitingAtPosition)
            {
                helperTimer -= Time.deltaTime;
                if (helperTimer <= 0)
                {
                    IsWaitingAtPosition = false;
                    SelectNewDestination();
                    isRotating = true;
                }
            }
            else if (isRotating)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(currentDirection), Time.deltaTime * 6);
                if (Quaternion.Angle(transform.rotation, Quaternion.LookRotation(currentDirection)) < 2)
                {
                    isRotating = false;
                    StartWalking();
                }
            }
            else
            {
                if (navAgent.pathStatus != NavMeshPathStatus.PathComplete)
                    SelectNewDestination();

                if (Vector3.Distance(transform.position, currentDestination) < 0.5f)
                {
                    ReachedDestination();
                }
            }
        }

        public virtual void ReachedDestination()
        {
            IsWaitingAtPosition = true;
            helperTimer = timeToSpendAtPosition;
            enemyAnimator.SetBool("IsWalking", false);
        }

        public void StartWalking()
        {
            enemyAnimator.SetBool("IsWalking", true);
            navAgent.SetDestination(currentDestination);
        }

        public void ForceRotation()
        {
            isRotating = true;
        }

        private void SelectNewDestination()
        {
            if (RouteType == EWalkingRouteType.RandomPositions)
            {
                AreaController areaToGo = LevelController.Get.AreasManager.GetRandomArea();
                if (areaToGo != null)
                {
                    currentDestination = areaToGo.transform.position;
                    currentDirection = (currentDestination - transform.position).normalized;
                    currentDirection.y = 0;
                }
            }
            else
            {
                if (definedPositions == null)
                    return;
                if (definedPositions.Count == 0)
                    return;

                int indexOfCurrentPosition = definedPositions.IndexOf(currentDestinationPosition);
                int nextIndex = (indexOfCurrentPosition + 1) % definedPositions.Count;
                currentDestinationPosition = definedPositions[nextIndex];
                currentDestination = currentDestinationPosition.position;
                currentDirection = (currentDestination - transform.position).normalized;
                currentDirection.y = 0;
            }


        }

        public void OnTriggerEnter(Collider other)
        {
            if (other == null)
                return;

            AreaController area = other.GetComponent<AreaController>();
            if (area == null)
                return;

            if (area.AreaStatus == EAreaStatus.TemporaryTakenByDrawing)
            {
                Debug.Log("Ha! Złapał Cię " + other.gameObject.name, other);
                LevelController.Get.PlayerCaught(EPlayerCaughtSource.EnemyCaughtWindScreens);
            }
        }

        public void Hit(float shockTime)
        {
            Debug.Log("Auć");
        }
    }

    public enum EWalkingRouteType
    {
        RandomPositions,
        DefinedPositions
    }
}