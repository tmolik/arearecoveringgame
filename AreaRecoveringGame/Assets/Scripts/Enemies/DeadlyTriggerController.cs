﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyTriggerController : MonoBehaviour
{
    public virtual void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        if (player != null)
        {
            LevelController.Get.PlayerCaught(EPlayerCaughtSource.DeadlyTrigger);
        }
    }
}
