﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(AreaController))]
public class AreaControllerEditor : Editor
{
    private AreaController areaControllerScript;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.Space();

        if (areaControllerScript == null)
            areaControllerScript = (AreaController)target;

        if (GUILayout.Button("Set as border area"))
        {
            if (serializedObject.isEditingMultipleObjects)
            {
                foreach (Object obj in serializedObject.targetObjects)
                {
                    AreaController areaController = (AreaController)obj;
                    if (areaController)
                        areaController.SetAsBorderAreaManually();
                }
            }
            else
            {
                areaControllerScript.SetAsBorderAreaManually();
            }
        }

        if (GUILayout.Button("Set as free area"))
        {
            if (serializedObject.isEditingMultipleObjects)
            {
                foreach (Object obj in serializedObject.targetObjects)
                {
                    AreaController areaController = (AreaController)obj;
                    if (areaController)
                        areaController.ResetAreaToFree();
                }
            }
            else
            {
                areaControllerScript.ResetAreaToFree();
            }
        }

        if (GUILayout.Button("Set as starting area"))
        {
            if(serializedObject.targetObject!=null)
            {
                AreaController area = (AreaController)serializedObject.targetObject;
                if (area != null)
                    area.SetAsStartingArea();

                EditorApplication.SaveScene();
            }
        }
        if (GUILayout.Button("Find neightbours"))
        {
            FindNeightbours();
        }

        GUILayout.Label("Special effects");
        if (GUILayout.Button("Add slow effect"))
        {
            AddEffect(EAreaSpecialEffectType.SlowEffect);
        }
        if (GUILayout.Button("Add trap effect"))
        {
            AddEffect(EAreaSpecialEffectType.TrapEffect);
        }
        if (GUILayout.Button("Add kill zone effect"))
        {
            AddEffect(EAreaSpecialEffectType.KillZoneEffect);
        }
        if (GUILayout.Button("Remove all effect"))
        {
            if (serializedObject.isEditingMultipleObjects)
            {
                foreach (Object obj in serializedObject.targetObjects)
                {
                    AreaController areaController = (AreaController)obj;
                    if (areaController)
                        areaController.ClearAllEffects();
                }
            }
            else
            {
                areaControllerScript.ClearAllEffects();
            }
        }
    }

    private void AddEffect(EAreaSpecialEffectType effectType)
    {
        if (serializedObject.isEditingMultipleObjects)
        {
            foreach (Object obj in serializedObject.targetObjects)
            {
                AreaController areaController = (AreaController)obj;
                if (areaController)
                    areaController.AddEffect(effectType);
            }
        }
        else
        {
            areaControllerScript.AddEffect(effectType);
        }
    }

    private void FindNeightbours()
    {
        if (serializedObject.isEditingMultipleObjects)
        {
            foreach (Object obj in serializedObject.targetObjects)
            {
                AreaController areaController = (AreaController)obj;
                if (areaController)
                    areaController.FindNeightbours();
            }
        }
        else
        {
            areaControllerScript.FindNeightbours();
        }
    }
}
