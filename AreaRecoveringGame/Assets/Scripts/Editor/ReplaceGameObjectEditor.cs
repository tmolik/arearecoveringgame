﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(ReplaceGameObject))]
public class ReplaceGameObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if(GUILayout.Button("Replace object"))
        {
            foreach(Object obj in serializedObject.targetObjects)
            {
                ReplaceGameObject replaceGameObjectScript = (ReplaceGameObject)obj;
                if (replaceGameObjectScript)
                    replaceGameObjectScript.Replace();
            }
        }
    }

}
