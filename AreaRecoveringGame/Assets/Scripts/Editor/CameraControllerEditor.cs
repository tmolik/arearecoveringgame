﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Cameras
{
    [CustomEditor(typeof(StaticCameraController))]
    public class CameraControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Center camera to areas"))
            {
                if (serializedObject.targetObject != null)
                {
                    StaticCameraController cameraScript = (StaticCameraController)serializedObject.targetObject;
                    if (cameraScript)
                        cameraScript.SetupCameraToBeInCenter();
                }
            }
        }
    }
}