﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace LevelBuilding
{
    [CustomEditor(typeof(LevelBuilderController))]
    public class LevelBuilderControllerEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
}