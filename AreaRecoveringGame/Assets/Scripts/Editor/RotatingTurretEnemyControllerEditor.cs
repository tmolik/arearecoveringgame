﻿using Enemies;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(RotatingTurretEnemyController))]
public class RotatingTurretEnemyControllerEditor : Editor
{
    private RotatingTurretEnemyController turretScript;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (turretScript == null)
            turretScript = (RotatingTurretEnemyController)target;

        EditorGUILayout.Space();

        if (GUILayout.Button("Clear defined rotation"))
        {
            turretScript.ClearDefinedRotations();
        }
        if(GUILayout.Button("Add current rotation to target rotations"))
        {
            turretScript.AddCurrentRotationToRotationTargets();
        }

    }
}
