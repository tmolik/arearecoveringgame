﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(KillZoneSpecialEffect))]
public class KillZoneSpecialEffectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Attach to KillZoneController"))
        {
            if (serializedObject.isEditingMultipleObjects)
            {
                foreach (Object obj in serializedObject.targetObjects)
                {
                    KillZoneSpecialEffect killZoneEffect = (KillZoneSpecialEffect)obj;
                    if (killZoneEffect)
                        killZoneEffect.AttachToKillZoneController();
                }
            }
            else
            {
                KillZoneSpecialEffect killZoneEffect = (KillZoneSpecialEffect)serializedObject.targetObject;
                if (killZoneEffect)
                    killZoneEffect.AttachToKillZoneController();
            }
        }
    }
}
