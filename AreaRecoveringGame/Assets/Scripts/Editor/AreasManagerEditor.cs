﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AreasManager))]
public class AreasManagerEditor : Editor
{
    private AreasManager areasManagerScript;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        EditorGUILayout.Space();
        if (areasManagerScript == null)
            areasManagerScript = (AreasManager)target;

        if (GUILayout.Button("Find areas in children"))
        {
            areasManagerScript.FindAreasInChildren();
        }
        if (GUILayout.Button("Set border areas"))
        {
            areasManagerScript.FindBorderAreas();
        }
        if (GUILayout.Button("Reset all areas to free"))
        {
            areasManagerScript.ResetAllAreas();
        }
        if (GUILayout.Button("Destroy all current areas and clear"))
        {
            areasManagerScript.DestroyAndClearAreas();
        }
    }
}
