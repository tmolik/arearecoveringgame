﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Levels
{
    [CustomEditor(typeof(LevelSelectController))]
    public class LevelSelectControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Create levels selectors"))
            {
                LevelSelectController levelSelectScript = (LevelSelectController)serializedObject.targetObject;
                if (levelSelectScript)
                    levelSelectScript.SpawnLevelsToSelect();
            }
        }
    }
}