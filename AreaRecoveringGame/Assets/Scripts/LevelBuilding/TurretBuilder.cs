﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelBuilding
{
    public class TurretBuilder : BaseBuilder
    {
        public override void OnMouseHold(Vector3 mousePositionInWorld)
        {
            base.OnMouseHold(mousePositionInWorld);
            Vector3 lookDirection = mousePositionInWorld - transform.position;
            lookDirection.y = 0;
            transform.rotation = Quaternion.LookRotation(lookDirection, Vector3.up);
        }
    }
}