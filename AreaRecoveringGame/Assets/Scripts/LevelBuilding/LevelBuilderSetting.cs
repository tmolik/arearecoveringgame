﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level Builder Setting", menuName = "Settings/Builder Setting")]
public class LevelBuilderSetting : ScriptableObject
{
    public int MaximumAdditionalBuildersCount = 64;

    public LayerMask BuildingRaycastsLayerMask;


    [SerializeField]
    private List<BuilderReference> builders = new List<BuilderReference>();

    public BuilderReference GetBuilder(EBuilderType builderType)
    {
        for (int i = 0; i <= builders.Count - 1; i++)
        {
            if (builders[i].BuilderType == builderType)
                return builders[i];
        }
        return null;

    }

    public BuilderReference GetNextBuilderReference(BuilderReference currentBuilder)
    {
        if (builders.IsLast(currentBuilder))
            return builders[0];
        else
            return builders.GetNext(currentBuilder);
    }

    public BuilderReference GetPreviousBuilderReference(BuilderReference currentBuilder)
    {
        if (builders.IndexOf(currentBuilder) == 0)
            return builders.Last();
        else
            return builders.GetPrevious(currentBuilder);
    }
}

[System.Serializable]
public class BuilderReference
{
    public EBuilderType BuilderType;
    public GameObject BuilderPrefab;
}