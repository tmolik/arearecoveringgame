﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelBuilding
{
    public class LevelBuilderController : MonoBehaviour
    {
        public static LevelBuilderController Get { get; private set; }

        public LevelBuilderController()
        {
            Get = this;
        }

        public LevelBuilderSetting LevelBuilderSettings = null;

        [SerializeField]
        private Transform builtGameObjectsParent = null;

        private BuilderReference currentBuilderReference;

        [SerializeField]
        private Camera mainCamera = null;

        [HideInInspector]
        public BaseBuilder MainEmptyBuilder;

        private bool builderLocked = false;
        private Vector3 currentMousePosition;

        private List<BaseBuilder> additionalBuilders = new List<BaseBuilder>();

        [SerializeField]
        private LevelBuilderOperationsController operationsController;

        public void Start()
        {
            ChangeCurrentBuilder(LevelBuilderSettings.GetBuilder(EBuilderType.AreaBuilder));
        }

        // Update is called once per frame
        void Update()
        {
            BuilderUpdate();
            CameraMovementInputUpdate();
            MouseInputUpdate();
            BuilderChangeInputUpdate();
        }

        private void ChangeCurrentBuilder(BuilderReference builderReference)
        {
            if (MainEmptyBuilder)
            {
                Destroy(MainEmptyBuilder.gameObject);
            }
            currentBuilderReference = builderReference;
            MainEmptyBuilder = null;
        }

        private void CreateEmptyBuilders()
        {
            if (currentBuilderReference == null)
                return;

            GameObject builderGameobject = Instantiate(currentBuilderReference.BuilderPrefab);
            MainEmptyBuilder = builderGameobject.GetComponent<BaseBuilder>();

            if (additionalBuilders.Count == 0)
            {
                for (int i = 0; i <= LevelBuilderSettings.MaximumAdditionalBuildersCount - 1; i++)
                {
                    GameObject additionalBuilderGameobject = Instantiate(LevelBuilderSettings.GetBuilder(EBuilderType.AreaBuilder).BuilderPrefab);
                    additionalBuilders.Add(additionalBuilderGameobject.GetComponent<BaseBuilder>());
                    additionalBuilderGameobject.transform.position = new Vector3(1000, 1000);
                    additionalBuilderGameobject.transform.SetParent(transform);
                }
            }
        }

        private void CameraMovementInputUpdate()
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            mainCamera.transform.position += new Vector3(horizontal, 0, vertical) * 2f * Time.deltaTime;
        }

        private void BuilderChangeInputUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                ChangeCurrentBuilder(LevelBuilderSettings.GetPreviousBuilderReference(currentBuilderReference));
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                ChangeCurrentBuilder(LevelBuilderSettings.GetNextBuilderReference(currentBuilderReference));
            }
        }

        private void BuilderUpdate()
        {
            if (MainEmptyBuilder == null)
            {
                CreateEmptyBuilders();
                return;
            }

            if (mainCamera == null)
                return;

            if (builderLocked)
            {
                LockedBuilderUpdate();
            }
            else
            {
                UnlockedBuilderUpdate();
            }
        }

        private void LockedBuilderUpdate()
        {
            if (!MainEmptyBuilder.CanBuiltMultipleObjects)
            {
                MainEmptyBuilder.OnMouseHold(currentMousePosition);
                return;
            }

            foreach (BaseBuilder builder in additionalBuilders)
            {
                builder.IsUsed = false;
                builder.transform.position = new Vector3(1000, 0, 1000);
            }
            MainEmptyBuilder.gameObject.SetActive(false);
            int xDistance = (int)Mathf.Abs(currentMousePosition.x - MainEmptyBuilder.transform.position.x);
            int zDistance = (int)Mathf.Abs(currentMousePosition.z - MainEmptyBuilder.transform.position.z);
            float xSign = Mathf.Sign(currentMousePosition.x - MainEmptyBuilder.transform.position.x);
            float zSign = Mathf.Sign(currentMousePosition.z - MainEmptyBuilder.transform.position.z);

            if (additionalBuilders.Count == 0)
                return;

            int buildersUsed = 0;
            for (int i = 0; i <= xDistance; i++)
            {
                for (int j = 0; j <= zDistance; j++)
                {
                    BaseBuilder builder = additionalBuilders[buildersUsed];
                    Vector3 builderPosition = MainEmptyBuilder.transform.position;
                    builderPosition += new Vector3(i * xSign, 0, j * zSign);
                    builder.transform.position = builderPosition;
                    builder.IsUsed = true;
                    buildersUsed++;
                    if (buildersUsed >= additionalBuilders.Count)
                        return;
                }
            }
        }

        private void UnlockedBuilderUpdate()
        {
            MainEmptyBuilder.transform.position = currentMousePosition;
        }

        private void MouseInputUpdate()
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 10;
            Vector3 position = mainCamera.ScreenToWorldPoint(mousePos);
            position.x = Mathf.Round(position.x);
            position.y = 0;
            position.z = Mathf.Round(position.z);
            currentMousePosition = position;

            if (Input.GetMouseButtonDown(0))
            {
                builderLocked = true;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (builderLocked)
                {
                    builderLocked = false;
                    BuildArea();
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                TryToDeleteHoveredObject();
                builderLocked = false;
            }
        }

        private void BuildArea()
        {
            if (MainEmptyBuilder == null)
                return;

            List<GameObject> builtGameObjects = new List<GameObject>();
            if (MainEmptyBuilder.CanBuiltMultipleObjects)
            {
                foreach (BaseBuilder builder in additionalBuilders)
                {
                    if (builder.IsUsed)
                    {
                        if (builder.CanBeBuilt())
                        {
                            GameObject builtGameObject = Instantiate(MainEmptyBuilder.ObjectsToBuildPrefabs.GetRandom());
                            builtGameObject.transform.position = builder.transform.position;
                            builtGameObject.transform.SetParent(builtGameObjectsParent);
                            builtGameObjects.Add(builtGameObject);
                        }
                    }

                    builder.transform.position = new Vector3(1000, 0, 1000);
                }
                MainEmptyBuilder.gameObject.SetActive(true);
            }
            else
            {
                GameObject builtGameObject = Instantiate(MainEmptyBuilder.ObjectsToBuildPrefabs.GetRandom());
                builtGameObject.transform.position = MainEmptyBuilder.transform.position;
                builtGameObject.transform.SetParent(builtGameObjectsParent);
                builtGameObjects.Add(builtGameObject);
            }

            operationsController.AddOperation(new CreateGameObjectsOperation(builtGameObjects));
        }

        private void TryToDeleteHoveredObject()
        {
            RaycastHit hit;
            Vector3 rayDirection = (currentMousePosition - mainCamera.transform.position).normalized;
            if (Physics.Raycast(mainCamera.transform.position, rayDirection, out hit, 40))
            {
                Destroy(hit.collider.gameObject);
            }
        }

    }
}