﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EBuilderType
{
    AreaBuilder,
    WalkingEnemyBuilder,
    GrenadeThrowingBuilder,
    TurretBuilder,
    RotatingTurretBuilder
}
