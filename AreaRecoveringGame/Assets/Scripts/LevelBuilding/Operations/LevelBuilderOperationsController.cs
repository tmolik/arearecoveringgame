﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBuilderOperationsController : MonoBehaviour
{
    private List<LevelBuildingOperation> previousOperations = new List<LevelBuildingOperation>();


    public void AddOperation(LevelBuildingOperation operation)
    {
        previousOperations.Add(operation);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
            TryToRestoreOperation();
    }

    private void TryToRestoreOperation()
    {
        if (previousOperations.Count == 0)
            return;
        previousOperations.Last().RestoreOperation();
        previousOperations.Remove(previousOperations.Last());
    }
}
