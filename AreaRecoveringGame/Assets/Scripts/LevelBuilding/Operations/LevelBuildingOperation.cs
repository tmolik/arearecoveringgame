﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LevelBuildingOperation 
{
    public abstract void RestoreOperation();
}
