﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateGameObjectsOperation : LevelBuildingOperation
{
    private List<GameObject> createdGameObjects = new List<GameObject>();
    
    public override void RestoreOperation()
    {
        foreach(GameObject gameObject in createdGameObjects)
        {
            GameObject.Destroy(gameObject);
        }
    }

    public CreateGameObjectsOperation(List<GameObject> createdGameObjects)
    {
        this.createdGameObjects = createdGameObjects;
    }
}
