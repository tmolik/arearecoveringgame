﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelBuilding
{
    public class BaseBuilder : MonoBehaviour
    {
        public List<GameObject> ObjectsToBuildPrefabs = new List<GameObject>();

        [HideInInspector]
        public bool IsUsed = false;

        public bool CanBuiltMultipleObjects = false;

        public bool CanBeBuilt()
        {
            Vector3 raycastOrigin = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);

            RaycastHit hit;
            if (Physics.Raycast(raycastOrigin, Vector3.down, out hit, 5, LevelBuilderController.Get.LevelBuilderSettings.BuildingRaycastsLayerMask))
            {
                return false;
            }
            return true;
        }

        public virtual void OnMouseHold(Vector3 mousePositionInWorld)
        {

        }
    }
}