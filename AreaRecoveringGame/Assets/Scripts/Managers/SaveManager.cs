﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public static SaveManager Get { get; private set; }

    public SaveManager()
    {
        Get = this;
    }

    private BinaryFormatter formatter = new BinaryFormatter();
    private bool saveInProgress = false;

    private const string savePref = "areaSave";

    private static GameData m_GameData;
    public static GameData GameData
    {
        get
        {
            if (m_GameData == null)
            {
                m_GameData = new GameData();
            }
            return m_GameData;
        }
        set
        {
            m_GameData = value;
        }
    }

    public static Action OnSave = delegate { };
    public static Action OnLoad = delegate { };

    private string loadPrefKey = "profileToLoad";

    private void Awake()
    {
        LoadGame();
    }

    [ContextMenu("Load Game")]
    public void LoadGame()
    {
        string loadedData = PlayerPrefs.GetString(savePref, string.Empty);

        //Debug.Log(loadedData);

        if (string.IsNullOrEmpty(loadedData) == false)
        {
            ReadFromBytes(Convert.FromBase64String(loadedData));
        }

        OnLoad.Invoke();
    }

    [ContextMenu("Save Game")]
    public void SaveGame()
    {
        if (saveInProgress == false)
        {
            StartCoroutine(SaveGameRoutine());
        }
    }

    private IEnumerator SaveGameRoutine()
    {
        saveInProgress = true;
        OnSave.Invoke();
        Debug.Log($"Saving...");

        yield return new WaitForSecondsRealtime(0.2f); //play loading wheel animation or something...

        PlayerPrefs.SetString(savePref, Convert.ToBase64String(ToBytes()));

        saveInProgress = false;
        //Debug.Log("Saved string " + Convert.ToBase64String(ToBytes()));
    }

    [ContextMenu("Delete Save")]
    public void DeleteSave()
    {
        if (PlayerPrefs.HasKey(savePref))
        {
            PlayerPrefs.DeleteKey(savePref);
        }
    }


    /// <summary>
    /// serialize game data to save or send
    /// </summary>
    /// <returns>The bytes.</returns>
    private byte[] ToBytes()
    {
        MemoryStream memStream = new MemoryStream();
        formatter.Serialize(memStream, GameData);
        byte[] buf = memStream.GetBuffer();
        memStream.Close();
        return buf;
    }

    /// <summary>
    /// Reads from bytes.
    /// </summary>
    /// <param name="b"serialized data</param>
    private void ReadFromBytes(byte[] b)
    {
        try
        {
            MemoryStream memStream = new MemoryStream(b);
            // Deserialize the hashtable from the file and 
            // assign the reference to the local variable.
            GameData loadedSave = (GameData)formatter.Deserialize(memStream);
            GameData = loadedSave;

            memStream.Close();
        }
        catch (SerializationException e)
        {
            Debug.Log("Failed to deserialize. Reason: " + e.Message);
            throw;
        }

    }
}
