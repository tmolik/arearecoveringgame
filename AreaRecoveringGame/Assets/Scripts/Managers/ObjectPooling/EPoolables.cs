﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EPoolables
{
    TurretBullet,
    CollectablesTransport,
    SpeedCollectable,
    SlowTimeCollectable,
    LifeCollectable,
    Grenade,
    InvulnerabilityCollectable,
    Removed,
    Explosion,
    SmokePuff,
    BulletHit,
    TurretBullet2,
    UiCollectableNotification,
    UmbrellaCollectable,
    OffensiveBallCollectable,
    OffensiveBallController,
    UiEffectIndicator
}
