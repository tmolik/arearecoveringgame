﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Collectables
{
    public class CollectablesManager : MonoBehaviour
    {
        [SerializeField]
        private List<ECollectableType> possibleCollectables = new List<ECollectableType>() { ECollectableType.Invulnerability, ECollectableType.Life, ECollectableType.SlowTime, ECollectableType.Speed, ECollectableType.Umbrella, ECollectableType.OffensiveBall };

        public void Start()
        {
            StartedLevel();
        }


        public void StartedLevel()
        {
            if (possibleCollectables.Count == 0)
                return;

            StopAllCoroutines();
            StartCoroutine(CollectablesSpawningRoutine());
        }

        public void FinishedLevel()
        {
            StopCoroutine(CollectablesSpawningRoutine());
        }

        private IEnumerator CollectablesSpawningRoutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(4, 10));
                SpawnCollectable();
            }
        }

        private void SpawnCollectable()
        {
            GameObject collectableTransport = ObjectPooler.Get.GetPooledObject(EPoolables.CollectablesTransport);
            if (collectableTransport == null)
                return;

            Vector3 randomPosition = AreasManager.Get.GetRandomArea().transform.position + Vector3.up * 10;
            collectableTransport.transform.position = randomPosition;

            CollectableTransportController transportController = collectableTransport.GetComponent<CollectableTransportController>();
            if (transportController != null)
            {
                ECollectableType collectable = possibleCollectables.GetRandom();
                transportController.CollectableType = collectable;
                transportController.Fall();
                transportController.gameObject.SetActive(true);
            }

        }
    }
}