﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpeedManager : MonoBehaviour
{
    public static GameSpeedManager Get { get; private set; }

    public GameSpeedManager()
    {
        Get = this;
    }

    [SerializeField]
    private float timeScaleChangeSpeed = 2f;

    private bool changingTimeScale;
    private float targetTimeScale;

    private float timeScaleBeforePause = 0f;
    public void PauseGame()
    {
        timeScaleBeforePause = Time.timeScale;
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        Time.timeScale = timeScaleBeforePause;
    }

    public void ChangeTime(float newTimeScale, float duration)
    {
        targetTimeScale = newTimeScale;
        changingTimeScale = true;
        StopAllCoroutines();
        StartCoroutine(ChangeTimeBack(duration));
    }

    public void Update()
    {
        if (changingTimeScale)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, targetTimeScale, Time.unscaledDeltaTime * timeScaleChangeSpeed);
            if (Mathf.Abs(Time.timeScale - targetTimeScale) < 0.05f)
                changingTimeScale = false;
        }
    }

    private IEnumerator ChangeTimeBack(float afterSeconds)
    {
        yield return new WaitForSecondsRealtime(afterSeconds);
        changingTimeScale = true;
        targetTimeScale = 1;

    }

}
