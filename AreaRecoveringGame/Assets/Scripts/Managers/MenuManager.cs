﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cameras;

public class MenuManager : MonoBehaviour
{

    private EMenuState _menuState;

    [SerializeField]
    private MenuCamera _menuCamera = null;

    [SerializeField]
    private GameObject _menuUI = null;

    public void Update()
    {
        if (_menuState == EMenuState.LevelSelect)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GoBackToMenu();
            }
        }
    }


    public void GoToLevelSelect()
    {
        if (_menuCamera.IsMoving)
            return;

        if (_menuState == EMenuState.Menu)
        {
            _menuUI.gameObject.SetActive(false);
            _menuState = EMenuState.LevelSelect;
            _menuCamera.GoToLevelSelect();
        }
    }

    public void GoBackToMenu()
    {
        if (_menuCamera.IsMoving)
            return;

        if (_menuState == EMenuState.LevelSelect)
        {
            _menuState = EMenuState.Menu;
            _menuCamera.GoToMenu();
        }
    }

    public void LeaveGame()
    {
        Application.Quit();
    }

    public void CameraBackOnPosition()
    {
        _menuUI.gameObject.SetActive(true);
    }
}

public enum EMenuState
{
    Menu,
    LevelSelect
}
