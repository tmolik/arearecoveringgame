﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AreaSpecialEffect : MonoBehaviour
{
    public abstract void PlayerEnteredArea(PlayerController player);

    public abstract void PlayerLeftArea(PlayerController player);

    public GameObject EffectGameObject;

}
