﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AreasManager : MonoBehaviour
{

    #region Singleton

    public static AreasManager Get { get; private set; }

    public AreasManager()
    {
        Get = this;
    }

    #endregion

    public AreasSpecialEffectsManager SpecialEffectsManager = null;

    [SerializeField]
    private GameObject AreaPrefab = null;

    public LayerMask AreasRaycastsLayerMask;

    [SerializeField]
    private List<AreaController> areas = new List<AreaController>();

    private Dictionary<int, List<AreaController>> GroupedFreeAreas = new Dictionary<int, List<AreaController>>();

    [ContextMenu("Clear areas")]
    public void DestroyAndClearAreas()
    {
        foreach (AreaController area in areas)
        {
            if (area == null)
                continue;
            DestroyImmediate(area.gameObject);
        }
        areas.Clear();
    }


    [ContextMenu("Set Border Areas")]
    public void FindBorderAreas()
    {
        for (int i = 0; i <= areas.Count - 1; i++)
        {
            areas[i].CheckIfIsBorder();
        }
    }

    [ContextMenu("Find areas in children")]
    public void FindAreasInChildren()
    {
        areas.Clear();
        foreach (AreaController area in GetComponentsInChildren<AreaController>())
        {
            if (area == null)
                continue;
            areas.Add(area);
        }
    }

    [ContextMenu("Reset all areas to free")]
    public void ResetAllAreas()
    {
        foreach (AreaController area in areas)
        {
            if (area == null)
                continue;
            area.ResetAreaToFree();
        }
    }

    public void UpdateWindScreenInAllAreas()
    {
        foreach (AreaController area in areas)
        {
            if (area == null)
                continue;
            area.UpdateWindScreenVisibility();
        }
    }

    public void PlayerFinishedTakingAreas()
    {
        GroupAreas();
        SelectAndTakeGroupedAreas();
        ResetAreasToFree(EAreaStatus.Checked);
        if (!AreThereFreeAreas())
        {
            LevelController.Get.GameWon();
        }
        UpdateWindScreenInAllAreas();
    }

    private void GroupAreas()
    {
        int checkNumber = 1;
        while (AreThereFreeAreas())
        {
            FindFirstFreeAreaAndCheckIt(checkNumber);
            checkNumber++;
        }

        List<int> groupsNeighbouringWithDrawedAreas = new List<int>();

        GroupedFreeAreas = new Dictionary<int, List<AreaController>>();
        foreach (AreaController area in areas)
        {
            if (area.AreaStatus == EAreaStatus.Checked)
            {
                if (!GroupedFreeAreas.ContainsKey(area.AreaNumber))
                {
                    GroupedFreeAreas[area.AreaNumber] = new List<AreaController>();
                }
                GroupedFreeAreas[area.AreaNumber].Add(area);

                if (!groupsNeighbouringWithDrawedAreas.Contains(area.AreaNumber))
                {
                    if (area.HasAnyNeightbourTakenByDrawing())
                        groupsNeighbouringWithDrawedAreas.Add(area.AreaNumber);
                }
            }
        }

        List<int> groupsToRemove = new List<int>();
        foreach (int areasGroupNumber in GroupedFreeAreas.Keys)
        {
            if (!groupsNeighbouringWithDrawedAreas.Contains(areasGroupNumber))
                groupsToRemove.Add(areasGroupNumber);
        }

        foreach (int groupToRemove in groupsToRemove)
        {
            GroupedFreeAreas.Remove(groupToRemove);
        }

    }

    private void SelectAndTakeGroupedAreas()
    {
        if (GroupedFreeAreas.Count == 1)
        {
            TakeDrawedAreas();
        }
        else
        {
            FindSmallestGroupAndTakeIt();
            TakeDrawedAreas();
        }
    }
    
    public void ResetAreasToFree(EAreaStatus areaStatusToReset)
    {
        for (int i = 0; i <= areas.Count - 1; i++)
        {
            if (areas[i].AreaStatus == areaStatusToReset)
            {
                areas[i].ResetAreaToFree();
            }
        }
    }

    private void FindSmallestGroupAndTakeIt()
    {
        int smallestGroupNumber = -1;
        int smallestGroupAreasCount = 100000;

        foreach (KeyValuePair<int, List<AreaController>> groupedArena in GroupedFreeAreas)
        {
            if (groupedArena.Value.Count < smallestGroupAreasCount)
            {
                smallestGroupAreasCount = groupedArena.Value.Count;
                smallestGroupNumber = groupedArena.Key;
            }
        }

        if (smallestGroupNumber != -1)
        {
            foreach (AreaController area in GroupedFreeAreas[smallestGroupNumber])
            {
                area.TakeArea();
            }
        }
    }

    public void TakeDrawedAreas()
    {
        for (int i = 0; i <= areas.Count - 1; i++)
        {
            if (areas[i].AreaStatus == EAreaStatus.TemporaryTakenByDrawing)
            {
                areas[i].TakeArea();
            }
        }
    }

    private bool AreThereFreeAreas()
    {
        int count = areas.Count(area => area.AreaStatus == EAreaStatus.Free);
        return count > 0;
    }


    public void FindFirstFreeAreaAndCheckIt(int checkNumber)
    {
        for (int i = 0; i <= areas.Count - 1; i++)
        {
            if (areas[i].AreaStatus == EAreaStatus.Free)
            {
                areas[i].CheckThisAreaAndNeightbours(checkNumber);
                return;
            }
        }
    }

    public AreaController GetRandomArea()
    {
        return areas.GetRandom();
    }

    public Vector3 GetAreasCenterPosition()
    {
        Vector3 centerPosition = Vector3.zero;
        for (int i = 0; i < areas.Count; i++)
        {
            centerPosition += areas[i].transform.position;
        }
        centerPosition /= areas.Count;
        return centerPosition;
    }

    public List<AreaController> GetAreasInDistanceRange(Vector3 firstPoint, float minDistance, float maxDistance)
    {
        List<AreaController> areasInRange = new List<AreaController>();
        for (int i = 0; i < areas.Count; i++)
        {
            float distance = Vector3.Distance(areas[i].transform.position, firstPoint);
            if (distance <= maxDistance && distance >=minDistance)
                areasInRange.Add(areas[i]);
        }

        return areasInRange;
    }
}
