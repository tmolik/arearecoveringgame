﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Special Effects", menuName = "Settings/Area special effects list")]
public class AreaSpecialEffectsList : ScriptableObject
{
    [SerializeField]
    private List<SpecialEffectPrefabReference> specialEffectsPrefabs = new List<SpecialEffectPrefabReference>();

    public SpecialEffectPrefabReference GetPrefabReference(EAreaSpecialEffectType effectType)
    {
        for (int i = 0; i < specialEffectsPrefabs.Count; i++)
        {
            if (specialEffectsPrefabs[i].EffectType == effectType)
                return specialEffectsPrefabs[i];
        }
        return null;
    }

}

[System.Serializable]
public class SpecialEffectPrefabReference
{
    public EAreaSpecialEffectType EffectType;
    public Vector3 LocalPositionOnArea;
    public Vector3 FinalScale = Vector3.one;
    public GameObject EffectPrefab;
}
