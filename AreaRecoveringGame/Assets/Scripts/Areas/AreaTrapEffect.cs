﻿using Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrapEffect : AreaSpecialEffect
{
    [SerializeField]
    private Animator trapAnimator=null;

    private bool usedOnce = false;

    [SerializeField]
    private float activeTime = 4f;
    [SerializeField]
    private float notActiveTime = 2f;

    private bool isActive = false;
    private float activeTimer = 0f;

    public void Start()
    {
        SetActive();
        activeTimer = Random.Range(0, activeTimer);
    }


    public override void PlayerEnteredArea(PlayerController player)
    {
        if (!isActive)
            return;

        if (trapAnimator == null)
            trapAnimator = EffectGameObject.GetComponent<Animator>();

        if (LevelController.Get != null)
            LevelController.Get.PlayerCaught(EPlayerCaughtSource.Trap);

        SetNotActive();
    }

    public override void PlayerLeftArea(PlayerController player)
    {
    }

    public void Update()
    {
        activeTimer -= Time.deltaTime;
        if(activeTimer <= 0)
        {
            if (isActive)
                SetNotActive();
            else
                SetActive();
        }
    }


    private void SetActive()
    {
        if (trapAnimator == null)
            trapAnimator = EffectGameObject.GetComponent<Animator>();

        isActive = true;
        trapAnimator.CrossFade("Spread",0.25f);
        activeTimer = activeTime;
    }

    private void SetNotActive()
    {
        if (trapAnimator == null)
            trapAnimator = EffectGameObject.GetComponent<Animator>();
            
        isActive = false;
        trapAnimator.CrossFade("Close", 0.1f);
        activeTimer = notActiveTime;
    }

}
