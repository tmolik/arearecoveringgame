﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaSlowEffect : AreaSpecialEffect
{
    [SerializeField]
    private float slowValue = 0.4f;

    public override void PlayerEnteredArea(PlayerController player)
    {
        player.SpecialEffectsController.EnteredSlowEffect(slowValue);
    }

    public override void PlayerLeftArea(PlayerController player)
    {
        player.SpecialEffectsController.LeftSlowEffect();
    }

}
