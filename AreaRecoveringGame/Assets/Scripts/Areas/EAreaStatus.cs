﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EAreaStatus
{
    Free,
    Checked,
    Taken,
    TemporaryTakenByDrawing,
    Border
}
