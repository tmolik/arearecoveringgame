﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreasSpecialEffectsManager : MonoBehaviour
{
    [SerializeField]
    private AreaSpecialEffectsList areasSpecialEffectsList;

    public void AddSpecialEffectToArea(AreaController area, EAreaSpecialEffectType effectType)
    {
        if (areasSpecialEffectsList == null)
            return;
        SpecialEffectPrefabReference prefabReference = areasSpecialEffectsList.GetPrefabReference(effectType);

        if (prefabReference == null)
            return;

        GameObject effectPrefab = prefabReference.EffectPrefab;
        if (effectPrefab == null)
            return;

        GameObject effectGameObject = Instantiate(effectPrefab);
        effectGameObject.transform.SetParent(area.transform);
        effectGameObject.transform.localScale = prefabReference.FinalScale;
        effectGameObject.transform.localPosition = prefabReference.LocalPositionOnArea;

        AddEffectComponent(area, effectType, effectGameObject);
    }

    private void AddEffectComponent(AreaController area, EAreaSpecialEffectType effectType, GameObject effectGameObject)
    {
        switch (effectType)
        {
            case EAreaSpecialEffectType.SlowEffect:
                AreaSlowEffect slowEffect = (AreaSlowEffect)area.gameObject.AddComponent(typeof(AreaSlowEffect));
                if (slowEffect != null)
                    slowEffect.EffectGameObject = effectGameObject;
                area.AreaEffects.Add(slowEffect);
                break;
            case EAreaSpecialEffectType.TrapEffect:
                AreaTrapEffect trapEffect = (AreaTrapEffect)area.gameObject.AddComponent(typeof(AreaTrapEffect));
                if (trapEffect != null)
                    trapEffect.EffectGameObject = effectGameObject;
                area.AreaEffects.Add(trapEffect);
                break;
            case EAreaSpecialEffectType.KillZoneEffect:
                KillZoneSpecialEffect killZoneEffect = (KillZoneSpecialEffect)area.gameObject.AddComponent(typeof(KillZoneSpecialEffect));
                if (killZoneEffect != null)
                    killZoneEffect.EffectGameObject = effectGameObject;
                area.AreaEffects.Add(killZoneEffect);
                break;
            default:
                break;
        }
    }
}


