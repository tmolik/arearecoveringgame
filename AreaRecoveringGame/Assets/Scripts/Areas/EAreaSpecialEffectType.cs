﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EAreaSpecialEffectType
{
    SlowEffect,
    TrapEffect,
    KillZoneEffect
}
