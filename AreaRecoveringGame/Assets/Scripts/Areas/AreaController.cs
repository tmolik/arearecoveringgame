﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaController : MonoBehaviour
{
    //[System.NonSerialized]
    public bool IsBorder;

    private MeshRenderer areaRenderer;
    [SerializeField]
    private Material borderMaterial = null;
    [SerializeField]
    private Material normalMaterial = null;
    [SerializeField]
    private Material takenMaterial = null;
    [SerializeField]
    private Material visitedMaterial = null;

    [SerializeField]
    private Transform windScreenTransform = null;

    [HideInInspector]
    public List<AreaController> NeightbourAreas = new List<AreaController>();

    [SerializeField]
    public List<AreaSpecialEffect> AreaEffects = new List<AreaSpecialEffect>();

    //[HideInInspector]
    public EAreaStatus AreaStatus;
    public int AreaNumber = 0;

    public void Start()
    {
        AreaNumber = 0;
    }

    public void CheckIfIsBorder()
    {
        FindNeightbours();

        IsBorder = NeightbourAreas.Count < 4;
        areaRenderer = GetComponent<MeshRenderer>();
        areaRenderer.material = IsBorder ? borderMaterial : normalMaterial;
        AreaStatus = IsBorder ? EAreaStatus.Border : EAreaStatus.Free;
    }


    public void FindNeightbours()
    {
        NeightbourAreas.Clear();
        List<Vector3> raycastsOrigins = new List<Vector3>();
        raycastsOrigins.Add(new Vector3(transform.position.x + transform.localScale.x, transform.position.y + 1, transform.position.z));
        raycastsOrigins.Add(new Vector3(transform.position.x - transform.localScale.x, transform.position.y + 1, transform.position.z));
        raycastsOrigins.Add(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z + transform.localScale.z));
        raycastsOrigins.Add(new Vector3(transform.position.x, transform.position.y + 1, transform.position.z - transform.localScale.z));

        int hits = 0;
        foreach (Vector3 raycastOrigin in raycastsOrigins)
        {
            RaycastHit hit;
            if (Physics.Raycast(raycastOrigin, Vector3.down, out hit, 5, AreasManager.Get.AreasRaycastsLayerMask))
            {
                AreaController neightbourArea = hit.collider.GetComponent<AreaController>();
                if (neightbourArea)
                {
                    NeightbourAreas.Add(neightbourArea);
                    hits++;
                }
            }
        }
    }

    [ContextMenu("Set as border area")]
    public void SetAsBorderAreaManually()
    {
        IsBorder = true;
        if (areaRenderer == null)
            areaRenderer = GetComponent<MeshRenderer>();
        areaRenderer.material = borderMaterial;
        AreaStatus = EAreaStatus.Border;
    }

    public void OnEnterArea(PlayerController player)
    {
        if (areaRenderer == null)
            areaRenderer = GetComponent<MeshRenderer>();
        areaRenderer.material = visitedMaterial;
        AreaStatus = EAreaStatus.TemporaryTakenByDrawing;

        windScreenTransform.gameObject.SetActive(true);
        windScreenTransform.rotation = Quaternion.LookRotation(Vector3.Cross(player.CurrentDirection, Vector3.up));
    }

    public void CheckAreaEffects(PlayerController player)
    {
        foreach (AreaSpecialEffect effect in AreaEffects)
        {
            effect.PlayerEnteredArea(player);
        }
    }

    public void OnLeftArea(PlayerController player)
    {
        foreach (AreaSpecialEffect effect in AreaEffects)
        {
            effect.PlayerLeftArea(player);
        }
    }

    public void TakeArea()
    {
        if (areaRenderer == null)
            areaRenderer = GetComponent<MeshRenderer>();
        areaRenderer.material = takenMaterial;
        AreaStatus = EAreaStatus.Taken;
    }

    public void CheckThisAreaAndNeightbours(int checkNumber)
    {
        AreaNumber = checkNumber;
        AreaStatus = EAreaStatus.Checked;

        foreach (AreaController area in NeightbourAreas)
        {
            if (area == null)
                continue;
            if (area.AreaStatus != EAreaStatus.Free)
                continue;
            area.CheckThisAreaAndNeightbours(checkNumber);
        }

    }

    public bool HasAnyNeightbourTakenByDrawing()
    {
        foreach (AreaController area in NeightbourAreas)
        {
            if (area == null)
                continue;
            if (area.AreaStatus == EAreaStatus.TemporaryTakenByDrawing)
                return true;
        }
        return false;
    }

    public void ResetAreaToFree()
    {
        AreaStatus = EAreaStatus.Free;
        IsBorder = false;
        if (areaRenderer == null)
            areaRenderer = GetComponent<MeshRenderer>();
        areaRenderer.material = normalMaterial;
        windScreenTransform.gameObject.SetActive(false);
    }

    public void AddEffect(EAreaSpecialEffectType effectType)
    {
        if (AreasManager.Get != null)
        {
            if (AreasManager.Get.SpecialEffectsManager != null)
            {
                AreasManager.Get.SpecialEffectsManager.AddSpecialEffectToArea(this, effectType);
            }
        }
    }


    public void ClearAllEffects()
    {
        foreach (AreaSpecialEffect effect in AreaEffects)
        {
            if (effect == null)
                continue;

            if (effect.EffectGameObject != null)
                DestroyImmediate(effect.EffectGameObject);
            DestroyImmediate(effect);
        }
        AreaEffects.Clear();
    }

    public void SetAsStartingArea()
    {
        if (Levels.LevelController.Get)
            Levels.LevelController.Get.SetAreaAsStarting(this);
    }

    public void UpdateWindScreenVisibility()
    {
        if (AreaStatus != EAreaStatus.Taken)
            return;

        foreach (AreaController area in NeightbourAreas)
        {
            if (area == null)
                continue;
            if (area.AreaStatus == EAreaStatus.Free)
            {
                windScreenTransform.gameObject.SetActive(true);
                Vector3 lookDirection = area.transform.position - transform.position;
                windScreenTransform.rotation = Quaternion.LookRotation(lookDirection.normalized);
                return;
            }
        }

        windScreenTransform.gameObject.SetActive(false);
    }


    void OnDrawGizmos()
    {
#if UNITY_EDITOR
        UnityEditor.Handles.Label(transform.position, AreaNumber.ToString());
#endif
    }

}
