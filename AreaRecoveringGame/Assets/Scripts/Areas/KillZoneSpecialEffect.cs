﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZoneSpecialEffect : AreaSpecialEffect
{
    [SerializeField]
    private KillZoneController killZoneController = null;

    [SerializeField]
    private GameObject killZoneParticlePrefab = null;

    [SerializeField]
    private int killZoneId = 0;

    public void OnEnable()
    {
        GameObject particle = Instantiate(killZoneParticlePrefab);
        particle.transform.SetParent(transform);
        particle.transform.localPosition = new Vector3(0, 0.1f, 0);
        particle.transform.localScale = Vector3.one * 0.3f;
    }

    public override void PlayerEnteredArea(PlayerController player)
    {
        if (killZoneController == null)
            return;

        killZoneController.PlayerEnteredKillZone(player);

    }

    public override void PlayerLeftArea(PlayerController player)
    {
        if (killZoneController == null)
            return;

        killZoneController.PlayerLeftKillZone(player);
    }

    public void AttachToKillZoneController()
    {
        foreach (KillZoneController killZone in FindObjectsOfType<KillZoneController>())
        {
            if (killZone == null)
                continue;
            if (killZone.KillZoneId == killZoneId)
            {
                killZoneController = killZone;
            }
        }

        if (killZoneController == null)
            CreateNewKillZoneAndAttach();
    }

    private void CreateNewKillZoneAndAttach()
    {
        GameObject killZoneGameObject = new GameObject($"KillZone{killZoneId}");
        KillZoneController killZone = (KillZoneController)killZoneGameObject.AddComponent(typeof(KillZoneController));
        if (killZone)
        {
            killZone.KillZoneId = killZoneId;
            killZoneController = killZone;
        }
    }

}
